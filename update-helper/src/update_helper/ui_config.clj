(ns update-helper.ui-config
  (:require [seesaw.color :refer [color]])
  (:import java.awt.Toolkit))


(def screen-size (.. Toolkit getDefaultToolkit getScreenSize))

(def is-4k-resolution? (and (> (.getWidth screen-size)
                               3000)
                            (> (.getHeight screen-size)
                               2000)))

(def stage-width (if is-4k-resolution?
                   400
                   250))
(def stage-height (if is-4k-resolution?
                    250
                    120))

(def stage-mid-screen-x (-
                         (/ (.getWidth screen-size)
                            2)
                         (/ stage-width 2)))

(def stage-mid-screen-y (-
                         (/ (.getHeight screen-size)
                            2)
                         (/ stage-height 2)))



(def main-color (color 5 153 153))
(def secondary-color (color 160 160 160))
(def background-color (color 250 250 250))
