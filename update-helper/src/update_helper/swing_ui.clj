(ns update-helper.swing-ui
  (:require [seesaw.core :as seesaw]
            [seesaw.cursor :refer [cursor]]
            [seesaw.style :refer [apply-stylesheet]]
            [seesaw.border :as border]
            [update-helper.ui-config :as config]))

(seesaw/native!)

(def styles-4k {[:#main-frame] {:resizable? false
                                :minimum-size [config/stage-width :by config/stage-height]}

                [:#main-panel] {:border (border/line-border :color config/secondary-color :thickness 3)
                                :background config/background-color}

                [:#title] {:foreground config/main-color
                           :font {:name "Roboto" :size 18}
                           :halign :center
                           :size [config/stage-width :by 30]
                           :bounds [10 10 (- config/stage-width 20) 40]}

                [:#progress-step-label] {:foreground :black
                                         :font {:name "Roboto" :size 17}
                                         :h-text-position :center
                                         :v-text-position :center
                                         :halign :center
                                         :size [config/stage-width :by 30]
                                         :border (border/line-border :color config/main-color :thickness 1)
                                         :bounds [10 60 (- config/stage-width 20) 100]}

                [:#progress-bar] {:bounds [10 162 (- config/stage-width 20) 60]
                                  :font {:name "Roboto" :size 17}
                                  :foreground config/main-color
                                  :border (border/line-border :color config/main-color :thickness 1)
                                  :paint-string? true}})

(def styles-normal {[:#main-frame] {:resizable? false
                                    :minimum-size [config/stage-width :by config/stage-height]}

                    [:#main-panel] {:border (border/line-border :color config/secondary-color :thickness 1)
                                    :background config/background-color}

                    [:#title] {:foreground config/main-color
                               :font {:name "Roboto" :size 12}
                               :halign :center
                               :size [config/stage-width :by 15]
                               :bounds [10 5 (- config/stage-width 20) 20]}

                    [:#progress-step-label] {:foreground :black
                                             :font {:name "Roboto" :size 11}
                                             :h-text-position :center
                                             :v-text-position :center
                                             :halign :center
                                             :size [config/stage-width :by 15]
                                             :border (border/line-border :color config/main-color :thickness 1)
                                             :bounds [10 30 (- config/stage-width 20) 50]}

                    [:#progress-bar] {:bounds [10 82 (- config/stage-width 20) 20]
                                      :font {:name "Roboto" :size 11}
                                      :foreground config/main-color
                                      :border (border/line-border :color config/main-color :thickness 1)
                                      :paint-string? true}})


(def title-label (seesaw/label :id :title
                               :text "Neue RedLorry Version"))

(def progress-step-label (seesaw/label :id :progress-step-label
                                       :text "Installation wird durchgeführt.."))

(def progress-bar (seesaw/progress-bar
                           :id :progress-bar
                           :max 200))

(def main-window (seesaw/frame
                  :id :main-frame
                  :title "Redlorry Updater"
                  :on-close :nothing
                  :content (seesaw/xyz-panel
                            :id :main-panel
                            :cursor (cursor :wait)
                            :items [title-label
                                    progress-step-label
                                    progress-bar])))

(defn update-download-progress [new-progress]
  (seesaw/config! progress-step-label
                  :text "Downloade neue Version..")
  (seesaw/config! progress-bar
                  :value new-progress))

(defn update-unzip-progress [new-progress]
  (seesaw/config! progress-step-label
                  :text "Entpacke neue Version..")
  (seesaw/config! progress-bar
                  :value (+ 100 new-progress)))

(defn update-finalize-progress [new-progress]
  (seesaw/config! progress-step-label
                  :text "Finalisiere Update..")
  (seesaw/config! progress-bar
                  :value 200))

(defn setup-window []
  (.setUndecorated main-window true)
  (.setAlwaysOnTop main-window true)
  (.setLocation main-window config/stage-mid-screen-x config/stage-mid-screen-y)
  (.setBackground main-window config/background-color)
  (apply-stylesheet main-window (if config/is-4k-resolution?
                                  styles-4k
                                  styles-normal)))

(defn start-ui []
  (setup-window)
  (seesaw/invoke-later
   (-> main-window
       seesaw/pack!
       seesaw/show!)))
