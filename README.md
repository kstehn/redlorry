# RedLorry

Ein Tool um in Redmine zu Buchen.

## Redmine
Before first run unzip the archive redmine-files.zip
```
$ unzip redmine-files.zip
```

To run redmine you need to install docker.
After this just run
```
$ docker-compose up -d
```

To shutdown
```
$ docker-compose down
```

### Infos

The data will be persisted in ./redmine-files

URL for redmine is: http://localhost:3000

Login: admin/password
API-Key: 6a4ff3011f0fbea322b78229316140edd50dd85b

Second Login: user/password
API-Key: a0909358778fe16c39812fec130c270d803c2b4e

There is one Project (Test-Project) with 4 Issues and two Querys.

There is one Project (Test-Project2) with 4 Issues and one Query.

## Requirements

* leiningen 2.7.x +
* node v8.12.x +
* make

Run the first time
```
$ make first install
```

## development mode

development mode use figwheel.
```
$ lein start
```
Open other terminal window.

```
$ lein electron
```
Works only for windows (for now)

For Linux
```
$ ./node_modules/.bin/electron ./resources/main.js
```

Debuging with devtron execute in console (in RedLorry)
```
require('devtron').install()
```
You should now see a Devtron tab added to the DevTools

## Package App

To build the release just execute:
```
$ make build
```
This creates a release in the folder out/<CURRENT_OS>

- Windows: RedLorry-win32-x64
- Linux: RedLorry-linux-x64
- Mac: RedLorry-darwin-x64

To make a release for all OS-System just execute:
```
$ make build-all
```

On Mac and Linux you need to have wine installed.
On Windows it can happen that this wont work because the mac release results in a error.
