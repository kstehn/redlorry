(ns redlorry.worker-core
  (:require [re-frame.core :as re-frame]
            [redlorry.util :refer [on-ipc]]
            [redlorry.worker-settings :as settings]
            [redlorry.redmine.issues :as issues]
            [redlorry.redmine.time-entries :as time-entries]))

(enable-console-print!)

(re-frame/reg-event-db
 ::initialize-db
 (fn  [_ _]
   {:queue-events {}
    :issues {}
    :time-entries {}}))

(on-ipc "update-settings" settings/update-settings)
(on-ipc "load-settings" settings/load-settings)
(on-ipc "load-issues" issues/load-issues)
(on-ipc "load-time-entries" time-entries/load-time-entry)
(on-ipc "read-unbooked-entries" time-entries/read-unbooked-entries)
(on-ipc "save-unbooked-entries" time-entries/save-unbooked-entries)

(defn -main []
  (re-frame/dispatch-sync [::initialize-db])
  (re-frame/clear-subscription-cache!))

(set! *main-cli-fn* -main)
