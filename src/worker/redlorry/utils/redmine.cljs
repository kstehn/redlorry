(ns redlorry.utils.redmine)

;====== Paths =======
(def root :redmine)
(def api-key-key :api-key)
(def url-key :url)
(def queue-infos :query-queue)
(def perv-query [queue-infos :prev-query])
(def cur-query [queue-infos :cur-query])

(def timeentries-infos :time-entries)
(def cur-timeentry-request [timeentries-infos :cur-request])

(defn issues
  "Query ID should be :all or a valid id from a query."
  [query-id]
  [:issues query-id])

(defn time-entries
  [period]
  [:time-entries period])