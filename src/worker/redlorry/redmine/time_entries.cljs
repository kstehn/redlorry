(ns redlorry.redmine.time-entries
  (:require [re-frame.core :as re-frame]
            [ajax.core :as ajax]
            [day8.re-frame.http-fx]
            [redlorry.utils.redmine :as u-redmine]
            [redlorry.utils.queue :as queue]
            [redlorry.util :refer [web-contents calc-offsets get-path send-all]]
            [redlorry.data :as data-util]))

(def queue-id ::time-entries)

(re-frame/reg-event-fx
 ::failed-request
 (fn [{db :db} [_ {:keys [url period assoc-id]} child-call resp]]
   (println "Request failed (" url "):" resp)
   (cond
     (and child-call
          (queue/more-in-queue? db queue-id)) {:db (queue/pop db queue-id)
                                               :dispatch (queue/dispatch-event db queue-id)}
     child-call {:dispatch [::done-loading period assoc-id]} ;Queue is empty and everything should be loaded
     (queue/more-in-queue? db queue-id) {:db (queue/pop db queue-id)
                                         :dispatch (queue/dispatch-event db queue-id)}
     :else {})))

(re-frame/reg-event-fx
 ::success-list-time-entries
 (fn [{db :db} [_ {:keys [period assoc-id] :as req-params} child-call req-id resp]]
   (let [time-entries (get resp :time_entries)
         curr-req-id (get-in db u-redmine/cur-timeentry-request)
         offset-steps (calc-offsets (get resp :limit)
                                    (get resp :total_count))
         ndb (if child-call
               (assoc-in db
                         (u-redmine/time-entries period)
                         (into (get-in db (u-redmine/time-entries period))
                               time-entries))
               (reduce (fn [new-db n-offset]
                         (queue/push new-db queue-id [::list-time-entries (assoc req-params :offset n-offset)
                                                      true req-id]))
                       (assoc-in db (u-redmine/time-entries period) time-entries)
                       offset-steps))]
      ;(info "Other time-entry infos:" time-entries-infos)
     (if (not= curr-req-id
               req-id)
       {:db (assoc-in db (u-redmine/time-entries req-id) nil)}
       (cond
         (and child-call
              (queue/more-in-queue? ndb queue-id)) {:db (queue/pop ndb queue-id)
                                                    :dispatch (queue/dispatch-event ndb queue-id)}
         child-call {:db ndb
                     :dispatch [::done-loading period assoc-id]} ;Queue is empty and everything should be loaded
         (queue/more-in-queue? ndb queue-id) {:db (queue/pop ndb queue-id)
                                              :dispatch (queue/dispatch-event ndb queue-id)}
         :else {:db ndb
                :dispatch [::done-loading period assoc-id]})))))

(re-frame/reg-event-fx
 ::list-time-entries
 (fn [_ [_ {:keys [api-key user-id url offset period spent-on-filter assoc-id] :as req-params} child-call req-id]]
   {:http-xhrio {:method :get
                 :uri url
                 :headers {:X-Redmine-API-Key api-key}
                 :params (if period
                           {:spent_on spent-on-filter
                            :limit 100
                            :offset offset
                            :user_id user-id}
                           {:limit 100
                            :offset offset
                            :user_id user-id})
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success [::success-list-time-entries req-params child-call req-id]
                 :on-failure [::failed-request req-params child-call]}}))

(re-frame/reg-event-fx
 ::done-loading
 (fn [{db :db} [_ period assoc-id]]
   (let [k-period (keyword period)
         time-entries (get-in db (u-redmine/time-entries k-period))]
     (send-all "time-entries-loaded" (clj->js {:assoc-id (if period
                                                           assoc-id
                                                           :all)
                                               :time-entries time-entries}))
     {:db (assoc-in db (u-redmine/time-entries k-period) [])})))

(re-frame/reg-event-db
 ::save-web-contents-caller
 (fn [db [_ caller]]
   (assoc db ::time-entry-caller caller)))

(re-frame/reg-event-fx
 ::check-and-clean-queue
 (fn [{db :db} [_ next-event]]
   (let [req-id (str (random-uuid))]
     {:db (-> db
              (queue/clean-queue queue-id)
              (assoc-in u-redmine/cur-timeentry-request req-id))
      :dispatch (conj next-event req-id)})))

(defn load-time-entry
  "Loads the time-entries."
  [evnet id params-map]
  (let [sender (aget evnet "sender")
        conv-params-map (js->clj params-map :keywordize-keys true)
        params (update conv-params-map :period (fn [old] (keyword old)))]
    (re-frame/dispatch [::check-and-clean-queue
                        [::list-time-entries params false]])
    (re-frame/dispatch [::save-web-contents-caller sender])))
(defn db-path
  "In Dev and Win: %APPDATA%/Electron
   In Prod and Win: %APPDATE%/redlorry"
  []
  (get-path "userData" "redlorry-entries.json"))

(defn save-unbooked-entries
  "Saves the unbooked entries to the filesystem"
  [event id entries]
  (let [conv-entries (js->clj entries :keywordize-keys true)]
    (data-util/write-path conv-entries (db-path))
    (send-all "done-saving-entries")))

(defn read-unbooked-entries
  "Reads the unbooked entries from the filesystem"
  [event id]
  (let [entries (clj->js (data-util/read (db-path)))]
    (send-all "done-reading-unbooked-entries" entries)))
