(ns redlorry.redmine.issues
    (:require [re-frame.core :as re-frame]
              [ajax.core :as ajax]
              [day8.re-frame.http-fx]
              [redlorry.utils.redmine :as u-redmine]
              [redlorry.utils.queue :as queue]
              [redlorry.util :refer [web-contents calc-offsets send-all]]))

(def queue-id ::issues)

(defn issues-query-id [{:keys [project-id query-id]}]
  (cond
    (and query-id project-id) (str project-id "-" query-id)
    query-id query-id
    project-id project-id
    :else :all))

(re-frame/reg-event-fx
 ::list-issues
 (fn [_ [_ api-key url offset limit {:keys [project-id query-id] :as args-map} child-call]]
   (let [params-map (cond-> {}
                      project-id (assoc :project_id project-id)
                      query-id (assoc :query_id query-id))]
     {:http-xhrio {:method :get
                   :uri url
                   :params (assoc params-map
                                  :limit limit
                                  :offset offset)
                   :headers {:X-Redmine-API-Key api-key}
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success [::success-list-issues api-key url offset limit args-map child-call]
                   :on-failure [::failed-request url]}})))

(re-frame/reg-event-fx
 ::success-list-issues
 (fn [{db :db} [_ api-key url _ limit args-map child-call resp]]
   (let [issues (:issues resp)
         issues-qid (issues-query-id args-map)
         cur-issues-qid (get-in db u-redmine/cur-query)
         offset-steps (calc-offsets (get resp :limit)
                                    (get resp :total_count))
         ndb (if child-call
               (assoc-in db 
                         (u-redmine/issues issues-qid) 
                         (into (get-in db (u-redmine/issues issues-qid))
                               issues))
               (reduce (fn [new-db n-offset]
                         (queue/push new-db queue-id [::list-issues api-key url n-offset limit args-map true]))
                       (assoc-in db (u-redmine/issues issues-qid) issues)
                       offset-steps))]
        (if (not (= cur-issues-qid
                    issues-qid)) ;if not equal the query was canceld from this call
          {:db (-> db
                   (assoc-in (u-redmine/issues issues-qid) nil))} ;remove previous loaded issues
          (cond
            (and child-call
                 (queue/more-in-queue? ndb queue-id)) {:db (queue/pop ndb queue-id)
                                                       :dispatch (queue/dispatch-event db queue-id)}
            child-call {:db ndb
                        :dispatch [::done-loading args-map]} ;Queue is empty and everything should be loaded
            (queue/more-in-queue? ndb queue-id) {:db (queue/pop ndb queue-id)
                                                 :dispatch (queue/dispatch-event ndb queue-id)}
            :else {:db ndb
                   :dispatch [::done-loading args-map]}))))) ;Nothing in queue

(re-frame/reg-event-fx
 ::done-loading
 (fn [{db :db} [_ args-map] ]
   (let [issues-qid (issues-query-id args-map)
         caller (get db ::issue-caller)
         issues (get-in db (u-redmine/issues issues-qid))]
     (send-all "issues-loaded" (clj->js {issues-qid issues}))
     {:db (assoc-in db (u-redmine/issues issues-qid) [])})))

(re-frame/reg-event-db
 ::save-web-contents-caller
 (fn [db [_ caller]]
   (assoc db ::issue-caller caller)))

(re-frame/reg-event-fx
 ::check-and-clean-queue
 (fn [{db :db} [_ args-map next-event]]
   (let [issues-qid (issues-query-id args-map)
         new-prev-query (get-in db u-redmine/cur-query)]
     {:db (-> db
              (queue/clean-queue queue-id)
              (assoc-in u-redmine/cur-query issues-qid)
              (assoc-in u-redmine/perv-query new-prev-query))
      :dispatch next-event})))

(defn load-issues
  "Loads the settings."
  [evnet id params-map]
  (let [sender (aget evnet "sender")
        conv-params-map (js->clj params-map :keywordize-keys true)
        {:keys [api-key url offset limit args-map]} conv-params-map]
    (re-frame/dispatch [::check-and-clean-queue
                        args-map 
                        [::list-issues api-key url offset limit args-map false]])
    (re-frame/dispatch [::save-web-contents-caller sender])))