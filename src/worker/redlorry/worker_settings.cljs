(ns redlorry.worker-settings
  (:require [redlorry.data :as data]
            [redlorry.util :refer [web-contents send-all]]))

(defn update-settings
  "Merge the given settings into stored settings for RedLorry"
  [event id settings]
  (let [contents (web-contents id)]
    (-> settings
        (js->clj :keywordize-keys true)
        data/write)))

(defn load-settings
  "Loads the settings."
  [event id settings]
  (let [settings (data/read)]
    (send-all "settings-loaded" (clj->js settings))))
