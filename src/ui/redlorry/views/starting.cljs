(ns redlorry.views.starting
 (:require [redlorry.api.redmine :as redmine]
           [re-frame.core :as re-frame]
           [redlorry.paths.redmine :as redmine-paths]
           [redlorry.views.settings.util :as sutil]
           [redlorry.util :refer [on-ipc get-url send-ipc]]
           [redlorry.views.settings.core :as ui-settings]
           [redlorry.worker :as worker]))

(enable-console-print!)

(def start-processes [:java-checked :settings-loaded :check-for-update :entries-loaded :init-load-try])

(re-frame/reg-event-fx
 ::system-conditions
 (fn [_ [_ res]]
  (worker/message! "load-settings")
  {:dispatch-n [[:redlorry.utils.system-conditions/system-conditions (js->clj res :keywordize-keys true)]
                [::finished-start-process :java-checked]]}))

(re-frame/reg-event-fx
 ::finished-start-process
 (fn [{db :db} [_ process]]
  (let [processes (get-in db [::start-processes] [])
        new-processes (conj processes process)]
      (if (or (get-in db [:start-ui] false)
              (= (set new-processes)
                 (set start-processes)))
        {:db (dissoc db ::start-processes
                        :redmine-tries)
         :dispatch [:redlorry.views.main-view/start-ui]}
        {:db (assoc-in db [::start-processes] new-processes)}))))

(defn settings-loaded [event settings]
 (let [settings (js->clj settings :keywordize-keys true)]
   (ui-settings/settings-loaded settings)
   (re-frame/dispatch [::finished-start-process :settings-loaded])
   ;Über Subscribe leider zu langsam, sodass hier nur nil rauskommen würde. TODO Evtl. auf Settings-util umbiegen, um den Pfad hier nicht hart drinne zu haben
   (if (and
        @(re-frame/subscribe [:redlorry.utils.system-conditions/updatefeature-available?])
        (get-in settings [:general :autoupdatecheck] true))
    (re-frame/dispatch [:redlorry.update-ui/run-check (fn [] (send-ipc "check-update"))
                                                     [::read-unbooked-entries]])
    (re-frame/dispatch [::read-unbooked-entries]))))

(re-frame/reg-event-fx
 ::read-unbooked-entries
 (fn [db]
   (worker/message! "read-unbooked-entries")
   {:dispatch [::finished-start-process :check-for-update]}))

(defn query-events [db entries]
 (let [query-ids (set (filterv #(not (nil? %))
                        (mapv (fn [{:keys [id query_id]}]
                               (let [qid (if query_id
                                           query_id
                                           id)
                                     issues? (not (nil? (get-in db
                                                                (redmine-paths/issues (str qid)))))]
                                (when-not issues?
                                 qid)))
                          entries)))
       query-maps (vec (map-indexed (fn [idx id]
                                      {:idx idx
                                       :q-id id
                                       :vec [::redmine/list-issues {:query-id (str id)}]})
                             query-ids))]
     query-maps))

(defn unbooked-entries-loaded [event entries]
 (let [entries (js->clj entries :keywordize-keys true)]
  (re-frame/dispatch [:redlorry.views.timetracking.tabs.booking/set-entries entries])
  (re-frame/dispatch [::finished-start-process :entries-loaded])
  (re-frame/dispatch [::init-load entries])))

(re-frame/reg-event-fx
 ::init-load
 (fn [{db :db} [_]]
  (let [[_ pid] (sutil/settings-for-key db :default-project)
        tries (get db :redmine-tries 0)]
   (if (get-in db (conj redmine-paths/connection-status :status))
     (let [dispatch-vecs (cond-> []
                           (empty? (get-in db redmine-paths/projects))
                           (conj {:ms 0 :dispatch [::redmine/list-projects]}
                                 {:ms 1000 :dispatch [::init-load]})

                           (and pid
                               (empty? (get-in db redmine-paths/queries)))
                           (conj {:ms 0 :dispatch [::redmine/list-queries pid]})

                           (empty? (get-in db redmine-paths/time-entry-activities))
                           (conj {:ms 0 :dispatch [::redmine/list-time-entry-activities]})

                           (and pid
                               (empty? (get-in db
                                               (redmine-paths/issues pid))))
                           (conj {:ms 0 :dispatch [::redmine/list-issues {:project-id (str pid)}]}
                                 {:ms 2000 :dispatch [::finished-start-process :init-load-try]}))] ; Etwas warten, damit Ergebnisse da])
      {:dispatch-later (if (and (empty? dispatch-vecs)
                                (nil? pid))
                          [{:ms 0 :dispatch [::finished-start-process :init-load-try]}]
                          dispatch-vecs)})
     {:db (assoc db :redmine-tries (inc tries))
      :dispatch-later [(when (< tries 5) {:ms 1000 :dispatch [::init-load]})
                       (when (> tries 4) {:ms 0 :dispatch [::finished-start-process :init-load-try]})]}))))
