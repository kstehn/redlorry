(ns redlorry.views.timetracking.tabs.all
  (:require [re-frame.core :as re-frame]
            [redlorry.paths.tabs :as paths]
            [redlorry.views.timetracking.utils :as tt-utils]
            [redlorry.utils.date :as u-date]
            [redlorry.utils.things :refer [sum-by-key]]
            [redlorry.material-ui :as mui]
            [redlorry.components.core :as components]))

(def granularity "all")

(re-frame/reg-event-fx
 ::req
 (fn [{db :db}]
   {:dispatch-n [(tt-utils/request-time-entries db granularity)]}))

(defn content []
  (let [entries  @(re-frame/subscribe [:redlorry.views.timetracking.utils/entries granularity])
        show-chart? @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key :booking-diagramm])]
    [mui/paper {:square true}
     (if entries
       [tt-utils/table-default entries]
       [:div {:style {:height "200px"}
              :align "center"}
        [components/loading "Daten werden geladen..."]])
     (when (and entries show-chart?)
       (let [labels (mapv #(:spent_on %)
                          (sort-by :spent_on entries))
             labels-formatted (mapv #(u-date/translate-to-new-date-str %
                                                                       tt-utils/source-date-string-format
                                                                       tt-utils/display-date-string-format)
                                    labels)
             data-grouped (group-by :spent_on entries)
             date-hours-vec (into {} (flatten (mapv (fn [[group entries]]
                                                      {group (sum-by-key entries [:hours])})
                                                    data-grouped)))
             datavec (mapv (fn [d]
                             (get date-hours-vec d 0))
                           labels)]
         [components/chart {:type :line
                            :labels labels-formatted
                            :data datavec}]))]))

(def tab-desc {paths/tab-id-key :all
               paths/tab-label-key "Alle"
               paths/tab-request-key [::req]
               paths/tab-content-key content})
