(ns redlorry.views.timetracking.tabs.week
  (:require [re-frame.core :as re-frame]
            [redlorry.paths.tabs :as paths]
            [redlorry.utils.date :as u-date]
            [redlorry.views.timetracking.utils :as timeentries-utils]
            [redlorry.utils.things :refer [sum-by-key]]
            [redlorry.material-ui :as mui]
            [redlorry.components.core :as components]))

(def granularity :week)

(re-frame/reg-sub
 ::selected
 (fn [db]
   (let [curr-day (.date (u-date/today))
         curr-week (+ 1 (js/Math.trunc (/ curr-day 7)))]
     (get-in db paths/request-week (str "Woche " curr-week)))))

(re-frame/reg-sub
 ::options
 (fn [db]
   (let [curr (u-date/today)
         curr-year (str (.year curr))
         curr-month-name (js/moment.months (.month curr))
         sel-year (get-in db paths/request-year curr-year)
         sel-month (get-in db paths/request-month curr-month-name)
         possible-weeks (u-date/month-weeks sel-month sel-year)]
     (mapv (fn [w]
             (str "Woche " w))
           possible-weeks))))

(re-frame/reg-event-fx
 ::switch
 (fn [{db :db} [_ sel]]
   (let [curr-day (.date (u-date/today))
         curr-week (+ 1 (js/Math.trunc (/ curr-day 7)))
         sel (or sel (get-in db paths/request-week (str "Woche " curr-week)))
         ndb (assoc-in db paths/request-week sel)]
     {:db ndb
      :dispatch-n [(timeentries-utils/request-time-entries ndb granularity)]})))

(defn selection-tabs []
  [components/tabs {:selected-sub [::selected]
                    :possible-tabs-sub [::options]
                    :select-event [::switch]}])

(defn content []
  (let [{:keys [entries assoc-id]} @(re-frame/subscribe [:redlorry.views.timetracking.utils/entries granularity true])
        [start-date] assoc-id
        show-bar-chart? @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key :booking-diagramm])]
    [mui/paper {:square true}
     [selection-tabs]
     (if entries
       [timeentries-utils/table-default entries]
       [:div {:style {:height "200px"}
              :align "center"}
        [components/loading "Daten werden geladen..."]])
     (when (and entries show-bar-chart?)
       (let [data-grouped (group-by :spent_on entries)
             date-hours-vec (into {} (flatten (mapv (fn [[group entries]]
                                                      {group (sum-by-key entries [:hours])})
                                                    data-grouped)))
             datavec (mapv (fn [d]
                             (get date-hours-vec d 0))
                           (u-date/get-week-dates start-date))]
         [components/chart {:type :bar
                            :labels (u-date/get-week-days true)
                            :data datavec
                            :dataset-label "Stunden für Tag"}]))]))

(def tab-desc {paths/tab-id-key granularity
               paths/tab-label-key "Woche"
               paths/tab-request-key [::switch]
               paths/selection-subscription [::selected]
               paths/tab-content-key content})
