(ns redlorry.views.timetracking.tabs.booking
  (:require [redlorry.material-ui :as mui]
            [re-frame.core :as re-frame]
            [redlorry.views.timetracking.utils :as tt-utils]
            [redlorry.utils.date :as u-date]
            [redlorry.paths.booking :as paths]
            [redlorry.paths.tabs :as tabs-paths]
            [redlorry.paths.redmine :as redmine-paths]
            [redlorry.utils.things :refer [get-unique-dom-key sum-by-key]]
            [redlorry.worker :as worker]))

(re-frame/reg-event-db
 ::sort-by
 (fn [db [_ by]]
   (let [direction (get-in db [:views :timetracking :tablesorting :direction] :desc)]
     (assoc-in db [:views :timetracking :tablesorting] {:direction (if (= direction
                                                                          :desc)
                                                                     :asc
                                                                     :desc)
                                                        :by by}))))

(re-frame/reg-sub
 ::sort-by
 (fn [db]
   (get-in db [:views :timetracking :tablesorting] {:direction :desc
                                                    :by :spent_on})))

(re-frame/reg-event-db
 ::save-entries-local
 (fn [db]
   (let [entries  (or (vals (get-in db paths/new-entry-rows))
                      [])]
     (worker/message! "save-unbooked-entries" (clj->js entries)))
   db))

(re-frame/reg-event-db
 ::select-all
 (fn [db [_ flag]]
   (let [flag (if (nil? flag)
                (or flag (not (get-in db paths/select-all-new-entries false)))
                flag)]
     (if flag
       (assoc-in db paths/select-all-new-entries true)
       (-> db
           (update-in paths/new-timeentries-section
                      dissoc
                      paths/timeentries-bookall)
           (update-in paths/new-timeentries-section
                      dissoc
                      paths/timeentries-tobook))))))

(re-frame/reg-sub
 ::should-select
 (fn [db]
   (get-in db paths/select-all-new-entries false)))

(re-frame/reg-event-db
 ::select
 (fn [db [_ entry flag]]
   (let [old-selected (get-in db paths/to-book-entries [])]
     (if flag
       (assoc-in db paths/to-book-entries (conj old-selected
                                                entry))
       (-> db
           (assoc-in paths/select-all-new-entries false)
           (assoc-in paths/to-book-entries
                     (filterv (fn [{:keys [eid]}]
                                (not (= eid (:eid entry))))
                              old-selected)))))))

(re-frame/reg-sub
 ::is-selected?
 (fn [db [_ sel-eid]]
   (not (nil? (some (fn [{:keys [eid]}]
                      (= sel-eid eid))
                    (get-in db paths/to-book-entries))))))

(re-frame/reg-sub
 ::selected-rows
 (fn [db]
   (get-in db paths/to-book-entries [])))

(re-frame/reg-event-fx
 ::entry-booked
 (fn [{db :db} [_ eid success?]]
   (if success?
     {:dispatch-n [[::select-all false]
                   [::delete-entry eid false]
                   (when (<= 1 (count (get-in db paths/to-book-entries)))
                     [:redlorry.components.status/show-status-message "Einträge erfolgreich gebucht" :success])
                   (when (<= 1 (count (get-in db paths/to-book-entries)))
                     [::save-entries-local])]}
     {:dispatch [:redlorry.components.status/show-status-message "Es konnte nicht gebucht werden" :error]})))

(defn invalidate-timeentries [db dates]
  (let [dates (set dates)
        timeentries-keys (keys (get-in db redmine-paths/time-entries-root))
        check-fn (fn [to-check]
                   (some (fn [k]
                           (if (vector? to-check)
                             (u-date/is-between? k (first to-check) (second to-check))
                             (= k to-check)))
                         dates))]
    (reduce (fn [ndb d]
              (if (or (and (string? d)
                           (= d "all"))
                      (check-fn d))
                (update-in ndb redmine-paths/time-entries-root dissoc d)
                ndb))
            db
            timeentries-keys)))

(re-frame/reg-event-fx
 ::book-all-selected
 (fn [{db :db}]
   (let [selected-entries (get-in db paths/to-book-entries)
         selected-dates (mapv :spent_on selected-entries)
         valid-timeentries (mapv (fn [{:keys [eid issue_id spent_on comments activity_id hours]}]
                                   [eid {:spent_on spent_on
                                         :issue_id issue_id
                                         :comments comments
                                         :hours hours
                                        ;:project {:id project_id}
                                         :activity_id activity_id}])
                                 selected-entries)]
     {:db (invalidate-timeentries db selected-dates)
      :dispatch [:redlorry.api.redmine/book-time-entries valid-timeentries]})))

(re-frame/reg-event-fx
 ::delete-all-selected
 (fn [{db :db}]
   (let [selected-entries (get-in db paths/to-book-entries)]
     {:db (reduce (fn [ndb {eid :eid}]
                    (update-in ndb paths/new-entry-rows dissoc eid))
                  db
                  selected-entries)
      :dispatch-n [[::select-all false]
                   [::save-entries-local]
                   [:redlorry.components.status/show-status-message "Einträge gelöscht" :info]]})))

(re-frame/reg-event-fx
 ::delete-entry
 (fn [{db :db} [_ eid message?]]
   {:db (update-in db paths/new-entry-rows dissoc eid)
    :dispatch-n [[::select {:eid eid} false]
                 [::save-entries-local]
                 (when message? [:redlorry.components.status/show-status-message "Eintrag gelöscht" :info])]}))

(re-frame/reg-sub
 ::issue-label
 (fn [db [_ issue_id project_id]]
   (let [issues (get-in db
                        (redmine-paths/issues project_id))]
     (:subject (first (filterv #(= (str (get % :id))
                                   (str issue_id))
                               issues))))))

(re-frame/reg-sub
 ::activity-text
 (fn [db [_ activity_id]]
   (let [activities (get-in db redmine-paths/time-entry-activities)]
     (:name (first (filterv #(= (str (get % :id))
                                (str activity_id))
                            activities))))))

(re-frame/reg-sub
 ::project-name
 (fn [db [_ project-id]]
   (let [projects (get-in db redmine-paths/projects)]
     (:name (first (filterv #(= (str (get % :id))
                                (str project-id))
                            projects))))))

(defn entry-row [entry]
  (let [dom-key (get-unique-dom-key)
        is-selected @(re-frame/subscribe [::is-selected? (:eid entry)])
        should-select @(re-frame/subscribe [::should-select])
        {:keys [eid spent_on hours comments issue_id activity_id query_id project_id] :as entry} entry
        issue-text @(re-frame/subscribe [::issue-label issue_id project_id])
        activity-text @(re-frame/subscribe [::activity-text activity_id])
        project-name @(re-frame/subscribe [::project-name project_id])]
    (when (and  should-select
                (not is-selected))
      (re-frame/dispatch [::select entry true]))
    [mui/table-row {:key (str "trow" dom-key)
                    :hover true
                    :role "checkbox"}

     [mui/table-cell {:key (str dom-key "editcheck" eid)
                      :style tt-utils/checkbox-cell-style}
      [mui/icon-button {:variant "fab"
                        :on-click (fn [e]
                                    (.stopPropagation e)
                                    (re-frame/dispatch [:redlorry.views.timetracking.entry-dialog/show-dialog true entry]))}

       [mui/edit-icon {:fontSize :small}]]
      [mui/checkbox {:checked is-selected
                     :on-click #(re-frame/dispatch [::select entry (not is-selected)])}]]
     [mui/table-cell {:key (str dom-key spent_on eid)
                      :style tt-utils/hours-cell-style}
      (u-date/translate-to-new-date-str spent_on
                                        tt-utils/source-date-string-format
                                        tt-utils/display-date-string-format)
      (when (:time-range? entry)
        [:br])
      (when (:time-range? entry)
        (str (:from entry) " - " (:to entry)))]
     [mui/table-cell {:key (str dom-key "hours" eid)
                      :style tt-utils/hours-cell-style}
      hours
      (when (and (:pause entry)
                 (not= "00:00" (:pause entry)))
        [:br])
      (when (and (:pause entry)
                 (not= "00:00" (:pause entry)))
        (str "(Pause: " (:pause entry) ")"))]
     [mui/table-cell {:key (str dom-key "issue" eid)
                      :style tt-utils/activity-cell-style}
      issue-text
      [:br]
      "(" project-name ")"]
     [mui/table-cell {:key (str dom-key "activity" eid)
                      :style tt-utils/activity-cell-style}
      activity-text]
     [mui/table-cell {:key (str dom-key "comment" eid)
                      :style tt-utils/comment-cell-style}
      comments]
     [mui/table-cell  {:key (str dom-key "delete" eid)
                       :style tt-utils/button-cell-style}
      [mui/icon-button {:variant "fab"
                        :on-click (fn [e]
                                    (.stopPropagation e)
                                    (re-frame/dispatch [:redlorry.components.confirm-dialog/show-dialog
                                                        true
                                                        [::delete-entry eid true]
                                                        "Eintrag wirklich löschen?"
                                                        "Möchtest du diesen Eintrag wirklich löschen?"]))}
       [mui/delete-icon {:fontSize :small}]]]]))

(defn sorting-cell
  ([label data-key cell-style sorting]
   (sorting-cell label nil data-key cell-style sorting))
  ([label second-label data-key cell-style sorting]
   (let [dom-key (get-unique-dom-key)]
     [mui/table-cell {:key (str dom-key (str data-key))
                      :style cell-style
                      :sortDirection (if (= (:by sorting)
                                            data-key)
                                       (name (:direction sorting))
                                       false)}
      [mui/table-sort-label {:active (= (:by sorting)
                                        data-key)
                             :direction (name (:direction sorting))
                             :on-click #(re-frame/dispatch [::sort-by data-key])}
       (if second-label
         [:div label
          [:br]
          second-label]
         label)]])))

(defn table-head [sorting]
  (let [dom-key (get-unique-dom-key)
        all-is-checked? @(re-frame/subscribe [::should-select])]
    [mui/table-head
     [mui/table-row {:key (str dom-key "thead")}
      [mui/table-cell {:key (str dom-key "editcheck")
                       :style tt-utils/checkbox-cell-style}
       [mui/checkbox {:checked all-is-checked?
                      :on-click #(re-frame/dispatch [::select-all (not all-is-checked?)])}]]

      [sorting-cell "Datum" :spent_on tt-utils/hours-cell-style sorting]
      [sorting-cell "Stunden" :hours tt-utils/hours-cell-style sorting]
      [sorting-cell "Buchungsposten" "(Projekt)" :issue_id tt-utils/activity-cell-style sorting]
      [sorting-cell "Aktivität" :activity_id tt-utils/activity-cell-style sorting]
      [sorting-cell "Kommentar" :comments tt-utils/comment-cell-style sorting]
      [mui/table-cell {:key (str dom-key "delete")
                       :style tt-utils/button-cell-style}

       ""]]]))

(defn foot-icon [{:keys [label icon title style]}]
  [mui/typography {:component "div"
                   :color :secondary
                   :style style}
   [mui/svg-icon {:font-size :small
                  :color "action"
                  :titleAccess title
                  :style {:position :absolute
                          :top "10px"}}
    [icon]]
   [:font {:style {:margin-left "25px"}}
    label]])

(defn table-footer [entries]
  (let [dom-key (get-unique-dom-key)
        additional-style {:display :inline
                          :margin-left "20px"
                          :font-size "13px"}
        hours-sum (sum-by-key entries [:hours])
        hours-sum-str (.toFixed hours-sum 2)
        dates-count (count (keys (group-by :spent_on entries)))
        average-hours-per-day (if (not= 0 dates-count)
                                (.toFixed (/ hours-sum dates-count)
                                          2)
                                0)
        entries-count (count entries)]
    [mui/paper {:key (str dom-key "table-footer")
                :square true
                :style {:width "100%"
                        :height "40px"
                        :position :relative}}
     [mui/svg-icon {:style {:padding-top "2px"
                            :padding-left "10px"
                            :display :inline}
                    :font-size :large
                    :color "secondary"}
      [mui/functions-icon]]
     [:div {:style {:display :inline-block
                    :position :absolute
                    :height "40px"
                    :padding-top "10px"}}
      [foot-icon
       {:label (str entries-count (if (= 1 entries-count)
                                    " Eintrag"
                                    " Einträge"))
        :icon mui/style-icon
        :title "Anzahl ungebuchte Einträge"
        :style additional-style}]
      [foot-icon
       {:label (str dates-count (if (= 1 dates-count)
                                  " Tag"
                                  " Tage"))
        :icon mui/event-available-icon
        :title "Anzahl ungebuchte Tage"
        :style additional-style}]
      [foot-icon
       {:label (str hours-sum-str " Stunden")
        :icon mui/timelapse-icon
        :title "Summe ungebuchte Stunden"
        :style additional-style}]
      [foot-icon
       {:label (str average-hours-per-day " Stunden")
        :icon mui/timer-icon
        :title "Stunden pro Tag (Durchschnitt)"
        :style additional-style}]]]))

(re-frame/reg-event-fx
 ::show-book-dialog
 (fn []
   {:dispatch [:redlorry.components.confirm-dialog/show-dialog
               true
               [::book-all-selected]
               "Einträge wirklich buchen?"
               "Möchtest du die markierten Einträge wirklich endgültig buchen?"]}))

(re-frame/reg-event-fx
 ::show-delete-dialog
 (fn []
   {:dispatch [:redlorry.components.confirm-dialog/show-dialog
               true
               [::delete-all-selected]
               "Einträge wirklich löschen?"
               "Möchtest du die markierten Einträge wirklich löschen?"]}))

(defn toolbar []
  (let [selected-rows @(re-frame/subscribe [::selected-rows])
        selected-num (count selected-rows)]

    [mui/toolbar
                    ;:style {:backgroundColor "#bad5d8"}}
     (if (> selected-num 0)
       [:div
        [mui/typography {:color "secondary"
                         :variant :subtitle1}
         (str selected-num " ausgewählt")]
        [mui/button {:variant "outlined"
                     :color "secondary"
                     :size :small
                     :on-click (fn [] (re-frame/dispatch [::show-delete-dialog]))
                     :style {:position :absolute
                             :top "0px"
                             :margin-top "15px"
                             :right 300}}
         [mui/delete-icon {:fontSize :small
                           :style {:margin-right "5px"}}]
         "Einträge Löschen"]
        [mui/button {:variant "outlined"
                     :color "primary"
                     :size :small
                     :on-click (fn [] (re-frame/dispatch [::show-book-dialog]))
                     :style {:position :absolute
                             :top "0px"
                             :margin-top "15px"
                             :right 100}}
         [mui/update-icon {:fontSize :small
                           :style {:margin-right "5px"}}]
         "Einträge Buchen"]]

       [:div
        [mui/typography {:variant "subtitle1"
                         :id "tableTitle"}
         "Einträge auswählen, die gebucht werden sollen"]
        [mui/button {:variant "outlined"
                     :color "primary"
                     :size :small
                     :on-click (fn [] (re-frame/dispatch [:redlorry.views.timetracking.entry-dialog/show-dialog true]))
                     :style {:position :absolute
                             :top "0px"
                             :margin-top "15px"
                             :right 100}}
         [mui/add-circle-icon {:fontSize :small
                               :style {:margin-right "5px"}}]
         "Neuer Eintrag"]])]))

(defn checkbox-table [entries]
  (let [sorting @(re-frame/subscribe [::sort-by])
        sorted-entries (if sorting
                         (vec (sort-by #(get % (:by sorting))
                                       (if (= (:direction sorting) :desc)
                                         >
                                         <)
                                       entries))
                         (vec entries))]
    [:div
     [toolbar]
     [mui/table
      [table-head sorting]
      [mui/table-body {:style {:display :none}}
       [mui/table-row]]]
     (if (or (nil? entries)
             (empty? entries))
       [:div]
       [:div {:style {:max-height "300px"
                      :overflow :auto}}
        [mui/table
         (reduce (fn [parent entry]
                   (conj parent (entry-row entry)))
                 [mui/table-body]
                 sorted-entries)]])
     [table-footer sorted-entries]]))

(re-frame/reg-event-db
 ::set-entries
 (fn [db [_ entries]]
   (reduce (fn [ndb entry]
             (assoc-in ndb (paths/new-entry-row (:eid entry)) entry))
           db
           entries)))

(re-frame/reg-sub
 ::new-entries
 (fn [db]
   (or (vals (get-in db paths/new-entry-rows))
       [])))

(defn content []
  (let [new-entries @(re-frame/subscribe [::new-entries])]
    [mui/paper {:square true}
     [checkbox-table new-entries]]))

(def tab-desc {tabs-paths/tab-id-key :booking
               tabs-paths/tab-label-key "Buchen"
               tabs-paths/tab-content-key content})
