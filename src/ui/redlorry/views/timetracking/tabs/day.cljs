(ns redlorry.views.timetracking.tabs.day
  (:require [re-frame.core :as re-frame]
            [redlorry.paths.tabs :as paths]
            [redlorry.utils.date :as u-date]
            [redlorry.views.timetracking.utils :as timeentries-utils]
            [redlorry.material-ui :as mui]
            [redlorry.components.core :as components]))

(def granularity :day)

(defn month-days [db]
  (let [curr (u-date/today)
        curr-year (str (.year curr))
        curr-month-name (js/moment.months (.month curr))
        sel-year (get-in db paths/request-year curr-year)
        sel-month (get-in db paths/request-month curr-month-name)]
    (u-date/days-of-month sel-month sel-year)))

(re-frame/reg-sub
 ::selected
 (fn [db]
   (let [curr-day (.date (u-date/today))]
     (get-in db paths/request-day curr-day))))

(re-frame/reg-sub
 ::disable?
 (fn [db [_ option]]
   (let [curr (u-date/today)
         curr-day (.date curr)
         curr-year (str (.year curr))
         curr-month-name (js/moment.months (.month curr))
         sel-year (get-in db paths/request-year curr-year)
         sel-month (get-in db paths/request-month curr-month-name)]

     (if (and (= sel-year curr-year)
              (= curr-month-name sel-month))
       (> option curr-day)
       false))))

(re-frame/reg-sub
 ::options
 (fn [db]
   (month-days db)))

(re-frame/reg-event-fx
 ::switch
 (fn [{db :db} [_ sel]]
   (let [sel (or sel (get-in db paths/request-day (.date (u-date/today))))
         ndb (assoc-in db paths/request-day sel)]
     {:db ndb
      :dispatch-n [(timeentries-utils/request-time-entries ndb granularity)]})))

(defn selection-tabs []
  [components/tabs {:selected-sub [::selected]
                    :min-width "10px"
                    :possible-tabs-sub [::options]
                    :disabled-sub [::disable?]
                    :select-event [::switch]}])

(defn content []
  (let [entries @(re-frame/subscribe [:redlorry.views.timetracking.utils/entries granularity])]
    [mui/paper {:square true}
     [selection-tabs]
     (if entries
       [timeentries-utils/table-default entries]
       [:div {:style {:height "200px"}
              :align "center"}
        [components/loading "Daten werden geladen..."]])]))

(def tab-desc {paths/tab-id-key granularity
               paths/tab-label-key "Tag"
               paths/tab-request-key [::switch]
               paths/selection-subscription [::selected]
               paths/tab-content-key content})
