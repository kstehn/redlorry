(ns redlorry.views.timetracking.tabs.month
  (:require [re-frame.core :as re-frame]
            [redlorry.paths.tabs :as paths]
            [redlorry.paths.redmine :as redmine-paths]
            [redlorry.utils.date :as u-date]
            [redlorry.views.timetracking.utils :as timeentries-utils]
            [redlorry.utils.things :refer [sum-by-key]]
            [redlorry.material-ui :as mui]
            [redlorry.components.core :as components]))

(def granularity :month)

(re-frame/reg-sub
 ::selected
 (fn [db]
   (let [curr-month (.month (u-date/today))
         curr-month-name (js/moment.months curr-month)]
     (get-in db paths/request-month curr-month-name))))

(re-frame/reg-sub
 ::disable?
 (fn [db [_ option]]
   (let [curr (u-date/today)
         curr-year (str (.year curr))
         curr-month (.month curr)
         sel-year (get-in db paths/request-year curr-year)
         all-months (u-date/get-months)]
     (if (= sel-year curr-year)
       (> (.indexOf all-months option)
          curr-month)

       false))))

(re-frame/reg-sub
 ::options
 (fn [db]
   (u-date/get-months)))

(re-frame/reg-event-fx
 ::switch
 (fn [{db :db} [_ sel]]
   (let [curr (u-date/today)
         curr-year (str (.year curr))
         curr-month (js/moment.months (.month curr))
         sel (or sel (get-in db paths/request-month curr-month))
         sel-year (get-in db paths/request-year curr-year)
         curr-day (.date curr)
         sel-day (get-in db paths/request-day curr-day)
         month-days (u-date/days-of-month sel sel-year)
         valid-date? (some #(= % sel-day) month-days)
         ndb (cond-> db
               :always (assoc-in paths/request-month sel)
               (and (= curr-year sel-year)
                    (= curr-month sel)
                    (> sel-day curr-day))
               (assoc-in paths/request-day curr-day)
               (and
                (not
                 (and (= curr-year sel-year)
                      (= curr-month sel)
                      (> sel-day curr-day)))
                (not valid-date?))
               (assoc-in paths/request-day (get month-days (- (count month-days) 1))))]
     {:db ndb
      :dispatch-n [(timeentries-utils/request-time-entries ndb granularity)]})))

(defn selection-tabs []
  [components/tabs {:selected-sub [::selected]
                    :possible-tabs-sub [::options]
                    :disabled-sub [::disable?]
                    :select-event [::switch]}])

(defn content []
  (let [entries @(re-frame/subscribe [:redlorry.views.timetracking.utils/entries granularity])
        show-bar-chart? @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key :booking-diagramm])]
    [mui/paper {:square true}
     [selection-tabs]
     (if entries
       [timeentries-utils/table-default entries]
       [:div {:style {:height "200px"}
              :align "center"}
        [components/loading "Daten werden geladen..."]])
     (when (and entries show-bar-chart?)
       (let [weeks-labels (mapv #(str "Woche " %)
                                (u-date/get-month-weeks))
             week-data (mapv (fn [entry]
                               (let [week (u-date/get-month-week-for-date (:spent_on entry))]
                                 (assoc entry :spent_on week)))
                             entries)
             data-grouped (group-by :spent_on week-data)
             week-hours-vec (into {} (flatten (mapv (fn [[group entries]]
                                                      {group (sum-by-key entries [:hours])})
                                                    data-grouped)))
             datavec (mapv (fn [w]
                             (get week-hours-vec w 0))
                           (u-date/get-month-weeks))]

         [components/chart {:type :bar
                            :labels weeks-labels
                            :data datavec
                            :dataset-label "Stunden für Woche"}]))]))

(def tab-desc {paths/tab-id-key granularity
               paths/tab-label-key "Monat"
               paths/tab-request-key [::switch]
               paths/selection-subscription [::selected]
               paths/tab-content-key content})
