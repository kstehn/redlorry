(ns redlorry.views.timetracking.tabs.year
  (:require [re-frame.core :as re-frame]
            [redlorry.paths.tabs :as paths]
            [redlorry.paths.redmine :as redmine-paths]
            [redlorry.utils.date :as u-date]
            [redlorry.views.timetracking.utils :as timeentries-utils]
            [redlorry.utils.things :refer [sum-by-key]]
            [redlorry.material-ui :as mui]
            [redlorry.components.core :as components]))

(def granularity :year)

(re-frame/reg-sub
 ::selected
 (fn [db]
   (let [curr-year (str (.year (u-date/today)))]
     (get-in db paths/request-year curr-year))))

(re-frame/reg-sub
 ::options
 (fn [db]
   (let [curr-year (.year (u-date/today))
         min-year 2017
         possible-years (range min-year (+ 1 curr-year))]
     (mapv str possible-years))))

(re-frame/reg-event-fx
 ::switch
 (fn [{db :db} [_ sel]]
   (let [curr (u-date/today)
         curr-year (str (.year curr))
         sel (or sel (get-in db paths/request-year curr-year))
         curr-month (.month curr)
         curr-day (.date curr)
         sel-month (get-in db paths/request-month (js/moment.months curr-month))
         sel-day (get-in db paths/request-day curr-day)
         month-days (u-date/days-of-month sel-month sel)
         valid-date? (some #(= % sel-day) month-days)
         ndb (cond-> db
               :always (assoc-in paths/request-year sel)
               (and (= curr-year sel)
                    (> (.indexOf (u-date/get-months) sel-month)
                       curr-month))
               (assoc-in paths/request-month (js/moment.months curr-month))
               (and (= curr-year sel)
                    (= curr-month sel-month)
                    (> sel-day curr-day))
               (assoc-in paths/request-day curr-day)
               (and
                (not
                 (and (= curr-year sel)
                      (= curr-month sel-month)
                      (> sel-day curr-day)))
                (not valid-date?))
               (assoc-in paths/request-day (get month-days (- (count month-days) 1))))]
     {:db ndb
      :dispatch-n [(timeentries-utils/request-time-entries ndb granularity)]})))

(defn selection-tabs []
  [components/tabs {:selected-sub [::selected]
                    :possible-tabs-sub [::options]
                    :select-event [::switch]}])

(defn content []
  (let [entries @(re-frame/subscribe [:redlorry.views.timetracking.utils/entries granularity])
        show-bar-chart? @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key :booking-diagramm])]
    [mui/paper {:square true}
     [selection-tabs]
     (if entries
       [timeentries-utils/table-default entries {:show-month-average true}]
       [:div {:style {:height "200px"}
              :align "center"}
        [components/loading "Daten werden geladen..."]])
     (when (and entries show-bar-chart?)
       (let [month-data (mapv (fn [entry]
                                (let [month  (u-date/get-month-name (:spent_on entry))]
                                  (assoc entry :spent_on month)))
                              entries)
             data-grouped (group-by :spent_on month-data)
             month-hours-vec (into {} (flatten (mapv (fn [[group entries]]
                                                       {group (sum-by-key entries [:hours])})
                                                     data-grouped)))
             datavec (mapv (fn [m]
                             (get month-hours-vec m 0))
                           (u-date/get-months))]
         [components/chart {:type :bar
                            :labels (u-date/get-months)
                            :data datavec
                            :dataset-label "Stunden für Monat"}]))]))

(def tab-desc {paths/tab-id-key granularity
               paths/tab-label-key "Jahr"
               paths/tab-request-key [::switch]
               paths/selection-subscription [::selected]
               paths/tab-content-key  content})
