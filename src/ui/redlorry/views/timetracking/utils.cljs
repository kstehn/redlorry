(ns redlorry.views.timetracking.utils
  (:require [re-frame.core :as re-frame]
            [redlorry.components.core :as components]
            [redlorry.utils.date :as u-date]
            [redlorry.material-ui :as mui]
            [clojure.string :as clj-str]
            [redlorry.api.redmine :as redmine-api]
            [redlorry.paths.tabs :as tabs-paths]
            [redlorry.paths.booking :as booking-paths]
            [redlorry.paths.redmine :as redmine-paths]))

(def source-date-string-format "YYYY-MM-DD")
(def display-date-string-format "DD.MM.YYYY")

(def checkbox-cell-style {:text-align "center"
                          :width "100px"
                          :padding "0px"})

(def button-cell-style {:text-align "center"
                        :width "30px"
                        :padding "0px 5px 0px 0px"})

(def date-cell-style {:text-align "left"
                      :width "66px"
                      :font-size 11
                      :padding "2px 20px 2px 20px"})

(def hours-cell-style {:text-align "center"
                       :font-size 11
                       :width "75px"
                       :padding "2px 20px 2px 20px"})

(def project-cell-style {:text-align "center"
                         :font-size 10
                         :width "86px"
                         :padding "2px 20px 2px 20px"})

(def issue-cell-style {:text-align "center"
                       :font-size 10
                       :width "106px"
                       :padding "2px 20px 2px 20px"})

(def activity-cell-style {:text-align "center"
                          :font-size 10
                          :width "86px"
                          :padding "2px 20px 2px 20px"})

(def comment-cell-style {:text-align "left"
                         :font-size 10
                         :padding "2px 26px 2px 20px"
                         :white-space "pre-wrap"})

(def time-entries-table-captions [{:data-key :spent_on
                                   :label "Datum"
                                   :type :date
                                   :style date-cell-style}
                                  {:data-key :hours
                                   :label "Stunden"
                                   :type :numeric
                                   :style hours-cell-style}
                                  {:data-key :project
                                   :label "Projekt"
                                   :type :string
                                   :style project-cell-style}
                                  {:data-key :issue
                                   :label "Buchungsposten"
                                   :type :string
                                   :style issue-cell-style}
                                  {:data-key :activity
                                   :label "Aktivität"
                                   :type :string
                                   :style activity-cell-style}
                                  {:data-key :comments
                                   :label "Kommentar"
                                   :type :string
                                   :style comment-cell-style}])

(defn build-table-entries [entries]
  (let [all-issues @(re-frame/subscribe [:redlorry.api.redmine/issues])]
    (mapv (fn [{:keys [spent_on hours comments activity issue project] :as entry}]
            (let [issue-text (:subject (first (filterv (fn [{:keys [id]}]
                                                         (= (:id issue)
                                                            id))
                                                       all-issues)))]
              {:id (random-uuid)
               :source-entry entry
               :spent_on {:sort-value spent_on
                          :value (u-date/translate-to-new-date-str spent_on
                                                                   source-date-string-format
                                                                   display-date-string-format)

                          :type :date
                          :style date-cell-style}
               :hours    {:sort-value hours
                          :value hours
                          :type :numeric
                          :style hours-cell-style}
               :project {:sort-value (:name project)
                         :value (:name project)
                         :type :string
                         :style project-cell-style}
               :issue {:sort-value issue-text
                       :value issue-text
                       :type :string
                       :style issue-cell-style}
               :activity {:sort-value (:name activity)
                          :value (:name activity)
                          :type :string
                          :style activity-cell-style}
               :comments {:sort-value comments
                          :value comments
                          :type :string
                          :style comment-cell-style}}))
          entries)))

(defn table-default
  ([entries props]
   [components/table (merge {:captions time-entries-table-captions
                             :entries (build-table-entries entries)}
                            props)])
  ([entries]
   [components/table {:captions time-entries-table-captions
                      :entries (build-table-entries entries)}]))

(defn valid-condition [diavalue can-be-nil?]
  (and (or can-be-nil?
           (and (not can-be-nil?)
                diavalue))
       (not (= diavalue ""))
       (not (= diavalue "00:00"))))

(re-frame/reg-sub
 ::project-options
 (fn [db [_ dlabel dval]]
   (let [projects (get-in db redmine-paths/projects)
         options (mui/vec->options (mapv (fn [{:keys [id name]}]
                                           {:key id :label name})
                                         projects))]
     (cond
       (and options (not-empty options))
       options
       (and dlabel dval)
       (mui/vec->options [{:label dlabel
                           :key (if dval
                                  (js/parseInt dval)
                                  0)}])
       :else []))))

(re-frame/reg-sub
 ::query-options
 (fn [db [_ path dlabel dval]]
   (let [selected-project (get-in db path)
         sel-project-id (when (and selected-project (not= "" selected-project))
                          (int selected-project))
         queries (get-in db redmine-paths/queries)
         project-queries (if sel-project-id
                           (filterv (fn [{:keys [project_id]}]
                                      (or
                                       (nil? project_id)
                                       (= sel-project-id
                                          project_id)))
                                    queries)
                           queries)
         options (mui/vec->options (mapv (fn [{:keys [id name]}]
                                           {:key id :label name})
                                         project-queries))]
     (cond
       (and options (not-empty options))
       options
       (and dlabel dval)
       (mui/vec->options [{:label dlabel
                           :key (if dval
                                  (js/parseInt dval)
                                  0)}])
       :else []))))

(re-frame/reg-sub
 ::activity-options
 (fn [db [_ dlabel dval]]
   (let [activities (get-in db redmine-paths/time-entry-activities)
         options (mui/vec->options (mapv (fn [{:keys [id name]}]
                                           {:key id :label name})
                                         activities))]
     (cond
       (and options (not-empty options))
       options
       (and dlabel dval)
       (mui/vec->options [{:label dlabel
                           :key (if dval
                                  (js/parseInt dval)
                                  0)}])
       :else []))))

(re-frame/reg-sub
 ::issue-options
 (fn [db [_ p-path q-path]]
   (let [project (get-in db p-path)
         project-id (int project)
         query (get-in db q-path)
         query-id (if (valid-condition query false)
                    (int query)
                    :all)
         request-id (if (and query-id project-id)
                      (str project-id "-" query-id)
                      project-id)
         issues (get-in db (redmine-paths/issues request-id))
         options (mui/vec->options (mapv (fn [{:keys [id subject]}]
                                           {:key id :label subject})
                                         issues))]
     (cond
       (and options (not-empty options))
       options
       :else []))))

(defn request-db-identifier [db granularity]
  (let [{y tabs-paths/year-key
         m tabs-paths/month-key ;Month String
         d tabs-paths/day-key
         w tabs-paths/week-key
         :or {y (.year (u-date/today))
              d (.date (u-date/today))}} ;Week String
        (get-in db tabs-paths/request)
        m  (if m
             (.indexOf (u-date/get-months)
                       m)
             (.month (u-date/today)))
        w (if w
            (js/parseInt (clj-str/replace w #"Woche " ""))
            (+ 1 (js/Math.trunc (/ (.date (u-date/today) 7)))))]
    (cond
      (= granularity "all")
      "all"
      (= granularity :day)
      (u-date/day d m y)
      (= granularity :month)
      (u-date/month-range m y)
      (= granularity :week)
      (u-date/week-range w m y)
      (= granularity :year)
      (u-date/year-range y))))

(defn entries [db granularity send-key?]
  (cond
    (= granularity :booking)
    (or (vec (vals (get-in db booking-paths/new-entry-rows)))
        [])
    (= granularity :all)
    (get-in db (redmine-paths/time-entries "all"))
    :else
    (let [assoc-id (request-db-identifier db granularity)
          entries (get-in db (redmine-paths/time-entries assoc-id))]
      (if send-key?
        {:entries entries
         :assoc-id assoc-id}
        entries))))

(re-frame/reg-sub
 ::entries
 (fn [db [_ granularity send-key?]]
   (entries db granularity send-key?)))


(defn request-time-entries [db granularity]
  (let [custom-value (request-db-identifier db granularity)
        data-exist? (boolean (get-in db (redmine-paths/time-entries custom-value)))
        period (cond
                 (= granularity :day)
                 :custom-day
                 (not= granularity "all")
                 :custom-period)]
    (cond
      (and period custom-value (not data-exist?))
      [:redlorry.api.redmine/list-perdiod-time-entries period custom-value custom-value]
      (and (nil? period)
           custom-value
           (not data-exist?))
      [:redlorry.api.redmine/list-all-time-entries custom-value])))

(defn complete-data [db data]
  (mapv (fn [entry]
          (let [{:keys [issue project_id query_id issue_id activity_id activity project]} entry
                project_id (when project_id (js/parseInt project_id))
                query_id (when query_id (js/parseInt query_id))
                issue_id (when issue_id (js/parseInt issue_id))
                activity_id (when activity_id (js/parseInt activity_id))]
            (cond-> entry
              activity (assoc :activity (get activity :name))
              project (assoc :project (get project :name))
              project_id (assoc :project (redmine-api/project-name db project_id))
              query_id (assoc :query (redmine-api/query-name db query_id))
              activity_id (assoc :activity (redmine-api/activity-name db activity_id))
              issue_id (assoc :issue (redmine-api/issue-name db issue_id))
              issue (assoc :issue (redmine-api/issue-name db (get issue :id))))))
        data))