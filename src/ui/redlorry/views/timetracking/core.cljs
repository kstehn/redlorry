(ns redlorry.views.timetracking.core
  (:require [re-frame.core :as re-frame]
            [reagent.core :as reagent]
            [redlorry.paths.tabs :as paths]
            [redlorry.paths.views :as views-paths]
            [redlorry.utils.date :as u-date]
            [redlorry.views.timetracking.entry-dialog :as entry-dia]
            [redlorry.views.timetracking.tabs.booking :as booking-view]
            [redlorry.views.timetracking.tabs.day :as day-view]
            [redlorry.views.timetracking.tabs.week :as week-view]
            [redlorry.views.timetracking.tabs.month :as month-view]
            [redlorry.views.timetracking.tabs.year :as year-view]
            [redlorry.views.timetracking.tabs.all :as all-view]
            [redlorry.views.timetracking.utils :as tt-utils]
            [redlorry.utils.xlsx-writer :as xlsx]
            [redlorry.material-ui :as mui]))

(def tabs (conj []
                booking-view/tab-desc
                day-view/tab-desc
                week-view/tab-desc
                month-view/tab-desc
                year-view/tab-desc
                all-view/tab-desc))

(re-frame/reg-event-fx
 ::switch-timeview
 (fn [{db :db} [_ selected-index]]
   {:db (assoc-in db paths/selected-tab selected-index)
    :dispatch-n [(get-in tabs (paths/tab-request selected-index))]}))

(re-frame/reg-sub
 ::current-timeview
 (fn [db _]
   (get-in db paths/selected-tab 0)))

(re-frame/reg-event-fx
 ::show-additonal-menu
 (fn [{db :db} [_ target flag callback]]
   {:db (if flag
          (assoc-in db [:views :timetracking :tabs :additional-menu] {:show? flag
                                                                      :target target})
          (update-in db [:views :timetracking :tabs] dissoc :additional-menu))
    :dispatch-later [(when callback
                       {:ms 500 :dispatch callback})]}))

(re-frame/reg-sub
 ::show-additonal-menu?
 (fn [db]
   (get-in db [:views :timetracking :tabs :additional-menu])))

(re-frame/reg-event-db
 ::show-export-options
 (fn [db [_ flag]]
   (if flag
     (assoc-in db [:views :timetracking :tabs :export-menu] true)
     (update-in db [:views :timetracking :tabs] dissoc :export-menu))))

(re-frame/reg-sub
 ::show-export-options?
 (fn [db]
   (get-in db [:views :timetracking :tabs :export-menu] false)))

(def booking-sheet-header
  [{:k :spent_on :label "Datum" :w 12 :sum-by :count}
   {:k :hours :label "Stunden" :w 12 :sum-by :sum}
   {:k :time-range? :label "Uhrzeiten?" :w 10}
   {:k :from :label "Von" :w 6}
   {:k :to :label "Bis" :w 6}
   {:k :pause :label "Pause" :w 6}
   {:k :project :label "Projekt" :w 25}
   {:k :query :label "Query" :w 20}
   {:k :issue :label "Buchungsposten" :w 25}
   {:k :activity :label "Aktivität" :w 25}
   {:k :comments :label "Kommentar" :w 100 :h-align "left" :wrap-text true}])

(def timeentries-sheet-header
  [{:k :spent_on :label "Datum" :w 12 :sum-by :count}
   {:k :hours :label "Stunden" :w 12 :sum-by :sum}
   {:k :project :label "Projekt" :w 25}
   {:k :issue :label "Buchungsposten" :w 25}
   {:k :activity :label "Aktivität" :w 25}
   {:k :comments :label "Kommentar" :w 100 :h-align "left" :wrap-text true}])

(re-frame/reg-event-db
 ::export
 (fn [db [_ export-type]]
   (let [curr-view-granularity (get-in tabs [(get-in db paths/selected-tab 0)
                                             paths/tab-id-key])
         header (if (= curr-view-granularity :booking)
                  booking-sheet-header
                  timeentries-sheet-header)
         data (tt-utils/entries db curr-view-granularity false)
         data (tt-utils/complete-data db data)]
     (xlsx/export
      {:data data
       :sheetname "Time Entries"
       :default-filename (str (.format (js/moment.)
                                       "YYYY-MM-DD")
                              "_timeentries")
       :header header
       :fail-event [:redlorry.components.status/show-status-message "Export fehlgeschlagen: Stelle sicher, dass die Datei nicht geöffnet ist" :error 5000]
       :cancel-event [:redlorry.components.status/show-status-message "Export abgebrochen" :info 3000]
       :success-event [:redlorry.components.status/show-status-message "Export erfolgreich" :success 1500]
       :export-type export-type
       :open-file? true})
     db)))

(defn custom-menu-item [label icon-path export-type]
  [mui/menu-item {:on-click (fn []
                              (re-frame/dispatch [::show-export-options false])
                              (re-frame/dispatch [::show-additonal-menu nil false [::export export-type]]))}
   [:img {:height "23px"
          :width "23px"
          :src icon-path
          :style {:margin-right "10px"}}]
   label])

(defn additonal-menu [show? target]
  (let [export-menu? @(re-frame/subscribe [::show-export-options?])
        curr-view @(re-frame/subscribe [::current-timeview])]
    [mui/menu {:open (true? show?)
               :id "additional-menu"
               :on-close #(re-frame/dispatch [::show-additonal-menu nil false])
               :anchorEl target
               :anchorOrigin {:vertical :top
                              :horizontal :right}
               :transformOrigin {:vertical :top
                                 :horizontal :right}}

     ;[mui/menu-item {:disabled true
     ;                  :on-click #(re-frame/dispatch [::show-additonal-menu nil false])}
     ;   "Einträge importieren"]
     ;[mui/divider]
     (when (= 0 curr-view)
       [mui/menu-item {:disabled true}
                     ;:on-click #(re-frame/dispatch [::show-additonal-menu nil false])}
        "Markierte Einträge vereinen"])
     (when (= 0 curr-view)
       [mui/divider])
     [mui/menu-item {:style {:padding-left "10px"}
                     :on-click #(re-frame/dispatch [::show-export-options (not export-menu?)])}
      [mui/save-icon {:style {:margin-right "10px"}}]
      "Einträge exportieren"
      (if export-menu?
        [mui/expand-less-icon {:style {:margin-left "15px"}}]
        [mui/expand-more-icon {:style {:margin-left "15px"}}])]
     [mui/collapse {:in export-menu?
                    :timeout "auto"
                    :unmountOnExit true}
      [:div {:style {:margin-left "20px"}}
       [custom-menu-item "Excel" "img/xlsx-icon.png" :excel]
       [custom-menu-item "CSV" "img/csv-icon.png" :csv]]]])

  #_[mui/menu-item  {:disabled true}
     :on-click #(re-frame/dispatch [::show-additonal-menu nil false])
     "Template laden"])

(re-frame/reg-event-fx
 ::reset-date
 (fn [{db :db}]
  (let [selected-index (get-in db paths/selected-tab 0)
        curr (u-date/today)
        curr-day (.date curr)
        curr-month-name (js/moment.months (.month curr))
        curr-year (str (.year curr))
        curr-week (+ 1 (js/Math.trunc (/ curr-day 7)))]
    {:db (-> db
             (assoc-in paths/request-day curr-day)
             (assoc-in paths/request-week (str "Woche " curr-week))
             (assoc-in paths/request-month curr-month-name)
             (assoc-in paths/request-year curr-year))
     :dispatch-n [(get-in tabs (paths/tab-request selected-index))]})))

(defn content-header [current-timeview]
  (let [value @(re-frame/subscribe [::current-timeview])
        show-menu? @(re-frame/subscribe [::show-additonal-menu?])
        dproject @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key :default-project])
        disable-tabs? (not (and dproject
                                (not-empty (first dproject))))]
    [mui/paper {:square true
                :style {:display :flex
                        :flex-direction :row}}
     [:div {:style {:display :flex
                    :flex-direction :column}}
      (apply conj
             [mui/tabs {:textColor "secondary"
                        :indicatorColor "secondary"
                        :style {:min-height "0px"}
                        :value value
                        :on-change (fn [_ value]
                                     (re-frame/dispatch [::switch-timeview value]))}]
             (mapv (fn [{:keys [label sel-sub]}]
                     (let [selection-value (when sel-sub
                                             @(re-frame/subscribe sel-sub))]
                       [mui/tab (cond-> {:label label
                                         :disabled disable-tabs?
                                         :style {:min-width "100px"
                                                 :min-height "0px"}}
                                  selection-value
                                  (assoc :icon (reagent/as-element
                                                [mui/typography
                                                 {:style {:font-size "8px"
                                                          :margin "0px"
                                                          :padding "0px"}}
                                                 selection-value])))]))
                   tabs))]
     [:div {:style {:display :flex
                    :flex-direction :column
                    :margin-top "10px"
                    :margin-left "30px"}}
      [mui/icon-button {:color "primary"
                        :disabled disable-tabs?
                        :size :small
                        :on-click #(re-frame/dispatch [::reset-date])}
       [mui/tooltip {:title "Datum zurücksetzen"}
                     ;:placement "top"} 
        [mui/refresh-icon]]]]
     [:div {:style {:display :flex
                    :flex-direction :column
                    :margin-top "10px"
                    :margin-left "20px"}}
      [mui/icon-button {:color "primary"
                        :disabled disable-tabs?
                        :aria-owns (when show-menu? "additional-menu")
                        :aria-haspopup "true"
                        :size :small
                        :on-click #(re-frame/dispatch [::show-additonal-menu (aget % "currentTarget")
                                                       true])}
       [mui/more-icon]]
      [additonal-menu
       (:show? show-menu?)
       (:target show-menu?)]]]))

(defn timetracking-view []
  (let [selected-index @(re-frame/subscribe [::current-timeview])]
    [:div.content
     [content-header
      (get-in tabs (paths/tab-label selected-index))]
     [entry-dia/dialog]
     [(get-in tabs (paths/tab-content selected-index))]]))

(defn content []
  (let [dproject @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key :default-project])]
    (when-not (and dproject
                   (not-empty (first dproject)))
      (re-frame/dispatch [:redlorry.components.status/show-status-message "Wähle ein Default-Projekt in den Einstellungen aus, um Buchen zu können" :warn 5000]))
    [timetracking-view dproject]))

(def view-desc {views-paths/view-id-key :timetracking
                views-paths/view-label-key "Tätigkeitserfassung"
                views-paths/view-icon-key [mui/accessalarm-icon {:font-size :small
                                                                 :color :disabled}]
                views-paths/view-content-key content})
