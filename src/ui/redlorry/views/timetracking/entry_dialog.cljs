(ns redlorry.views.timetracking.entry-dialog
 (:require [redlorry.material-ui :as mui]
           [re-frame.core :as re-frame]
           [reagent.core :as reagent]
           [redlorry.views.timetracking.utils :as tutil]
           [redlorry.utils.date :as date-util]
           [redlorry.paths.entry-dialog :as paths]
           [redlorry.paths.redmine :as redmine-paths]
           [redlorry.views.settings.util :as sutil]
           [redlorry.api.redmine :as redmine]))

(defn set-value [db root-path key value]
  (if (and
       (not (nil? value))
       (not (= "" value)))
   (assoc-in db (conj root-path key) value)
   db))

(defn set-dialog-values [db entry]
 (let [dialog-values-map (when entry
                           {paths/project-key (:project_id entry)
                            paths/issue-key (:issue_id entry)
                            paths/query-key (:query_id entry)
                            paths/activity-key (:activity_id entry)
                            paths/date-key (:spent_on entry)
                            paths/comment-key (:comments entry)
                            paths/hours-key (date-util/number->time-str (:hours entry))
                            paths/from-key (:from entry)
                            paths/to-key (:to entry)
                            paths/pause-key (:pause entry)
                            :time-range? (get entry :time-range? false)})]
  (if entry
    (assoc-in db paths/values-root dialog-values-map)
    (let [dproject (sutil/settings-for-key db :default-project)
          dquery (sutil/settings-for-key db :default-query)
          dactivity (sutil/settings-for-key db :default-activity)
          timerange-default (sutil/settings-for-key db :timerange-default)]
      (-> db
          (set-value paths/values-root
                     paths/project-key
                     (when dproject
                       (str (second dproject))))
          (set-value paths/values-root
                     paths/query-key
                     (when dquery
                       (str (second dquery))))
          (set-value paths/values-root
                     paths/activity-key
                     (when dactivity
                       (str (second dactivity))))
          (set-value paths/values-root
                     :time-range?
                     timerange-default))))))


(re-frame/reg-sub
 ::edit-mode?
 (fn [db]
   (not (nil? (get-in db paths/table-entry)))))

(re-frame/reg-sub
 ::show-dialog?
 (fn [db]
   (get-in db paths/new-dialog)))

(defn set-default [vals-vec keep-dialog-values key value]
  (if (and
       (not (nil? value))
       (not= "" value)
       (not (nil? vals-vec))
       (not-empty vals-vec)
       (or
        (nil? (get vals-vec key))
        (nil? (some (fn [e]
                      (= (:key e) key))
                   keep-dialog-values))))
     (assoc vals-vec key value)
    vals-vec))

(re-frame/reg-event-db
 ::reset-dialog
 (fn [db]
  (let [dproject (str (second (sutil/settings-for-key db :default-project)))
        dquery (str (second (sutil/settings-for-key db :default-query)))
        dactivity (str (second (sutil/settings-for-key db :default-activity)))
        keep-options (mapv (fn [o]
                             {:key (keyword (first o))})
                           (sutil/settings-for-key db :keep-options))
        keep-dialog-values-for-keys (conj (if keep-options
                                             keep-options
                                             [])
                                          {:key :date-range?}
                                          {:key :time-range?}
                                          {:key :from}
                                          {:key :to})
        new-dialog-values (into {} (filterv (fn [[dialog-key _]]
                                             (not (nil? (some #(= dialog-key (if (keyword? (:key %))
                                                                               (:key %)
                                                                               (keyword (:key %))))
                                                          keep-dialog-values-for-keys))))
                                        (get-in db paths/values-root)))
        dia-vals-defaults (-> new-dialog-values
                              (set-default keep-dialog-values-for-keys
                                           paths/project-key
                                           dproject)
                              (set-default keep-dialog-values-for-keys
                                           paths/query-key
                                           dquery)
                              (set-default keep-dialog-values-for-keys
                                           paths/activity-key
                                           dactivity))
        updated-dialog-values (if (and (:time-range? new-dialog-values)
                                       (:from new-dialog-values)
                                       (:to new-dialog-values))
                                (-> dia-vals-defaults
                                  (assoc :def-from (:to new-dialog-values)
                                         :def-to (date-util/number->time-str
                                                  (+ 1 (date-util/time-str->number (:to new-dialog-values)))))
                                  (dissoc :from :to))
                                dia-vals-defaults)]
    (assoc-in db paths/new-dialog {:show? true
                                   :values updated-dialog-values}))))

(defn build-entry [dialog-values old-id]
  (let [time-range? (get dialog-values :time-range?)]
    {:eid (if old-id
            old-id
            (str (random-uuid)))
     :project_id (str (get dialog-values paths/project-key))
     :issue_id (str (get dialog-values paths/issue-key))
     :query_id (str (get dialog-values paths/query-key))
     :activity_id (str (get dialog-values paths/activity-key))
     :spent_on (get dialog-values paths/date-key)
     :comments (get dialog-values paths/comment-key "")
     :from (get dialog-values paths/from-key "00:00")
     :to (get dialog-values paths/to-key "00:00")
     :pause (get dialog-values paths/pause-key "00:00")
     :time-range? time-range?
     :hours (if time-range?
              (- (date-util/time-str->number (get dialog-values paths/to-key))
                 (date-util/time-str->number (get dialog-values paths/from-key))
                 (date-util/time-str->number (get dialog-values paths/pause-key "00:00")))
              (- (date-util/time-str->number (get dialog-values paths/hours-key))
                 (date-util/time-str->number (get dialog-values paths/pause-key "00:00"))))}))

(re-frame/reg-event-fx
 ::update-entry
 (fn [{db :db} [_ entry]]
   (let [dialog-values (get-in db paths/values-root)
         updated-entry (build-entry dialog-values (:eid entry))]
    {:db (assoc-in db (paths/new-entry-row (:eid entry)) updated-entry)
     :dispatch-n [[::show-dialog false]
                  [:redlorry.views.timetracking.tabs.booking/save-entries-local]
                  [:redlorry.components.status/show-status-message "Eintrag gespeichert" :info]]})))

(re-frame/reg-event-fx
 ::save-entry
 (fn [{db :db} [_ new-dialog?]]
  (let [dialog-values (get-in db paths/values-root)
        dates (get dialog-values paths/date-key)
        entry-template (build-entry dialog-values nil)
        entries (if (coll? dates)
                  (mapv (fn [d]
                         (assoc entry-template
                          :eid (str (random-uuid))
                          :spent_on (str (.getFullYear d) "-"
                                         (if (> 10 (+ 1 (.getMonth d)))
                                           (str "0" (+ 1 (.getMonth d)))
                                           (+ 1 (.getMonth d)))
                                         "-"
                                         (if (> 10 (.getDate d))
                                           (str "0" (.getDate d))
                                           (.getDate d)))))
                      (vals dates))
                  [entry-template])]
    {:db (reduce (fn [ndb entry]
                    (assoc-in ndb (paths/new-entry-row (:eid entry)) entry))
            db
            entries)
     :dispatch-n [[:redlorry.components.status/show-status-message (if (coll? dates)
                                                                     "Einträge angelegt"
                                                                     "Eintrag angelegt")
                                                                  :info]
                  [:redlorry.views.timetracking.tabs.booking/save-entries-local]
                  (if new-dialog?
                   [::reset-dialog]
                   [::show-dialog false])]})))


(defn valid-date-condition [diavalue can-be-nil? multi-dates?]
 (and (or can-be-nil?
          (and (not can-be-nil?)
               diavalue))
      (not (= diavalue ""))
      (not (= diavalue "00:00"))
      (or (and multi-dates?
               (coll? diavalue)
               (not (empty? diavalue)))
          (and (not multi-dates?)
               (not (coll? diavalue))))))

(defn valid-time-condition [[h-key [time-range? f-key t-key p-key]] dialog-values]
 (or (and time-range?
        (get dialog-values f-key)
        (get dialog-values t-key)
        (< 0 (- (date-util/time-str->number (get dialog-values t-key))
                (date-util/time-str->number (get dialog-values f-key))
                (date-util/time-str->number (get dialog-values p-key "00:00")))))
     (and (not time-range?)
          (get dialog-values h-key)
          (< 0 (- (date-util/time-str->number (get dialog-values h-key))
                  (date-util/time-str->number (get dialog-values p-key "00:00")))))))

(defn request-issues [db diakey diavalue]
 (when (and (or (= diakey paths/project-key)
                (= diakey paths/query-key))
            (tutil/valid-condition diavalue false))
   (let [project (if (= diakey paths/project-key)
                   diavalue
                   (get-in db paths/project-val))
         project-id (if (tutil/valid-condition project false)
                      (int project)
                      nil)
         query (if (= diakey paths/query-key)
                 diavalue
                 (get-in db paths/query-val))
         query-id (if (tutil/valid-condition query false)
                    (int query)
                    nil)]
     (if query-id
      [::redmine/list-issues {:project-id project-id
                              :query-id query-id}]
      [::redmine/list-issues {:project-id project-id}]))))

(re-frame/reg-event-fx
 ::show-dialog
 (fn [{db :db} [_ flag table-entry]]
  (if flag
    (let [newdb (-> db
                  (assoc-in paths/new-dialog {:show? flag
                                              :table-entry table-entry})
                  (set-dialog-values table-entry))]
     ;(js/console.log (request-issues newdb paths/project-key (get-in newdb paths/project-val)))              
     {:db newdb
      :dispatch (request-issues newdb paths/project-key (get-in newdb paths/project-val))})
    {:db (update-in db
                    paths/new-timeentries
                    dissoc
                    paths/dialog-key)})))

(re-frame/reg-event-db
 ::force-close
 (fn [db]
  (update-in db paths/new-dialog assoc :show? false)))

(re-frame/reg-event-fx
 ::update-dialog-values
 (fn [{db :db} [_ diakey diavalue]]
  {:db (assoc-in db (paths/dialog-value diakey) diavalue)
   :dispatch-n [(request-issues db diakey diavalue)]}))

(re-frame/reg-sub
 ::dialog-value
 (fn [db [_ diakey]]
   (get-in db (paths/dialog-value diakey))))

(re-frame/reg-sub
 ::dialog-value-valid?
 (fn [db [_ diakey]]
    (tutil/valid-condition (get-in db (paths/dialog-value diakey))
                    true)))

(defn dialog-values-valid? [db]
   (let [time-range? (get-in db (paths/dialog-value :time-range?) false)
          or-keys [paths/hours-key [time-range? paths/from-key paths/to-key paths/pause-key]]
          required [paths/project-key paths/query-key paths/issue-key paths/activity-key paths/date-key paths/comment-key or-keys]
          dialog-values (get-in db paths/values-root)
          multi-dates? (get-in db (paths/dialog-value :date-range?) false)]
    (every? true? (mapv (fn [diakey]
                         (cond
                          (= diakey paths/date-key)
                          (valid-date-condition (get dialog-values diakey)
                                                false
                                                multi-dates?)
                          (= diakey or-keys)
                          (valid-time-condition diakey dialog-values)
                          :else
                          (tutil/valid-condition (get dialog-values diakey)
                                           false)))
                   required))))

(re-frame/reg-sub
 ::dialog-values-valid?
 (fn [db]
  (dialog-values-valid? db)))

(re-frame/reg-sub
 ::project
 (fn [db]
   (get (first (filterv (fn [{id :id}]
                          (= (str id)
                             (str (get-in db paths/project-val))))
                 (get-in db redmine-paths/projects)))
       :name)))

(defn get-selection-val [ev]
  (aget ev "target" "value"))

(defn selection-input-row [default-value save-key label options helper-text]
 (let [is-valid-input? @(re-frame/subscribe [::dialog-value-valid? save-key])
       dialog-value @(re-frame/subscribe [::dialog-value save-key])
       value (if dialog-value
              dialog-value
              default-value)]
  [:div {:style {:display :flex
                 :flexDirection :row
                 :margin-bottom "15px"}}
      [mui/select-component
       {:id (str label "-choose")
        :name label
        :value value
        :is-valid-input? is-valid-input?
        :on-change-func #(re-frame/dispatch [::update-dialog-values save-key (get-selection-val %)])
        :options options
        :helper-text helper-text}]]))

(defn project-row []
 (let [project @(re-frame/subscribe [::project])]
  [:div {:style {:display :flex
                 :flexDirection :row
                 :margin-bottom "15px"}}
   [mui/typography {:variant "subtitle1"
                        :color "primary"
                        :style {:display :flex
                                :flex-grow 1
                                :flexDirection :column}}
     "Projekt:"]
   [mui/typography {:variant "subtitle1"
                        :color "primary"
                        :style {:display :flex
                                :flex-grow 1
                                :flexDirection :column}}
     project]]))

(defn query-row []
 (let [default-value ""
       options @(re-frame/subscribe [:redlorry.views.timetracking.utils/query-options
                                     paths/project-val])]
  [selection-input-row default-value
                       paths/query-key
                       "Query"
                      options
                     ""]))

(defn issue-row []
  (let [default-value ""
        options  @(re-frame/subscribe [:redlorry.views.timetracking.utils/issue-options
                                       paths/project-val
                                       paths/query-val])]
   [selection-input-row default-value
                        paths/issue-key
                        "Buchungsposten"
                       options
                       "Auswahl abhängig von Projekt und Query"]))

(defn activity-row []
  (let [default-value ""
        options @(re-frame/subscribe [:redlorry.views.timetracking.utils/activity-options])]
   [selection-input-row default-value
                        paths/activity-key
                        "Aktivität"
                       options
                      ""]))

(re-frame/reg-sub
 ::date-range?
 (fn [db]
  (get-in db (paths/dialog-value :date-range?) false)))

(re-frame/reg-event-fx
 ::date-range
 (fn [{db :db} [_ flag set-default-value-event]]
  {:db (assoc-in db (paths/dialog-value :date-range?) flag)
   :dispatch-n [(when (not flag)
                  set-default-value-event)]}))

(defn date-row []
 (let [is-valid-input? @(re-frame/subscribe [::dialog-value-valid? paths/date-key])
       default-value (date-util/d-format (date-util/today))
       dialog-value @(re-frame/subscribe [::dialog-value paths/date-key])
       set-default-value-event [::update-dialog-values paths/date-key default-value]
       value (if dialog-value
              dialog-value
              default-value)
       date-range?  @(re-frame/subscribe [::date-range?])
       show-switch? (not @(re-frame/subscribe [::edit-mode?]))]
  (when-not dialog-value
    (re-frame/dispatch set-default-value-event)) ;Default Wert speichern
  [:div {:style {:display :flex
                 :flexDirection (if show-switch?
                                  :row
                                  :column)
                 :width "100%"
                 :margin-bottom "15px"}}
   (when show-switch?
    [:div {:style {:display :flex
                   :flexDirection :column}}
     [mui/form-group
      [mui/form-control-label
       {:control (reagent/as-element [mui/switch {:size :small
                                                  :checked date-range?
                                                  :onChange #(re-frame/dispatch [::date-range (not date-range?)
                                                                                 set-default-value-event])}])
        :label (reagent/as-element
                [mui/typography {:style {:font-size "12px"}}
                 "Reiheneintrag?"])}]]])
   ;[:br]
   [:div {:style {:display :flex
                  :flexDirection :column}}

    (if date-range?
       (let [dates-map (if (coll? value)
                          value
                          {})]
        [:div {:style {:width "100%"
                       :font-family "Arial"
                       :height "200px"}}
         [:> js/window.DayPicker.
              {:numberOfMonths 1
               :firstDayOfWeek 1
               :showWeekNumbers true
               :selectedDays (vals dates-map)
               :months (date-util/get-months)
               :weekdaysLong (date-util/get-week-days false)
               :weekdaysShort (date-util/get-week-days-short false)
               :on-day-click (fn [day]
                               (let [selected (first (filterv (fn [[_ val]]
                                                                 (= val day))
                                                           dates-map))
                                     new-selected (if selected
                                                    (dissoc dates-map (first selected))
                                                    (assoc dates-map (str (random-uuid))
                                                                     day))]

                                 (re-frame/dispatch [::update-dialog-values
                                                     paths/date-key
                                                     (if new-selected
                                                       new-selected
                                                       {})])))}]])
       [mui/textfield {:type "date"
                           :fullWidth true
                           :error (not is-valid-input?)
                           :label "Datum"
                           :on-change #(re-frame/dispatch [::update-dialog-values paths/date-key (aget % "target" "value")])
                           :defaultValue (if (coll? value)
                                             default-value
                                             value)
                           :InputLabelProps {:shrink true}
                           :InputProps {:startAdornment (reagent/as-element [mui/input-adornment {:position :start}
                                                                             [mui/event-icon {:fontSize :small}]])}}])]]))
(defn comment-row []
 (let [default-value ""
       dialog-value @(re-frame/subscribe [::dialog-value paths/comment-key])
       value (if dialog-value
              dialog-value
              default-value)]
   [:div {:style {:display :flex
                  :flexDirection :row
                  :margin-bottom "15px"}}
    [mui/textfield {:label "Kommentar"
                    :fullWidth true
                    :id "entry-dialog-comment"
                    :multiline true
                    :rowsMax 6
                    :defaultValue value
                    :on-change #(re-frame/dispatch [::update-dialog-values paths/comment-key (aget % "target" "value")])
                    :InputProps {:startAdornment (reagent/as-element [mui/input-adornment {:position :start}
                                                                      [mui/comment-icon {:fontSize :small}]])}}]]))

(defn time-field
 ([default-value data-key label icon helperText required?]
  [time-field default-value data-key label icon helperText required? nil])
 ([default-value data-key label icon helperText required? init-vec]
  (let [is-valid-input? @(re-frame/subscribe [::dialog-value-valid? data-key])
        dialog-value @(re-frame/subscribe [::dialog-value data-key])]
    (when (and (not dialog-value)
               init-vec)
      (doseq [v init-vec]
        (re-frame/dispatch v)))
    [mui/textfield  {:type "time"
                        :error (and
                                required?
                                (not is-valid-input?))
                        :helperText helperText
                        :defaultValue (if dialog-value
                                        dialog-value
                                        default-value)
                        :label label
                        :fullWidth true
                        :on-change (fn [ev]
                                     (re-frame/dispatch [::update-dialog-values data-key (aget ev "target" "value")]))
                        :inputProps {:step 900
                                     :min 0}
                        :InputProps {:startAdornment (reagent/as-element [mui/input-adornment {:position :start}
                                                                          (if icon
                                                                            icon
                                                                            [:div])])}}])))
(re-frame/reg-sub
 ::time-range?
 (fn [db]
  (get-in db (paths/dialog-value :time-range?) false)))

(re-frame/reg-event-fx
 ::time-range
 (fn [{db :db} [_ flag set-default-value-event]]
  {:db (assoc-in db (paths/dialog-value :time-range?) flag)
   :dispatch-n (if (not flag)
                 set-default-value-event
                 [])}))

(defn hours-row []
 (let [default-value "00:00"
       from-def @(re-frame/subscribe [::dialog-value :def-from])
       settings-from-default @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key :default-start-working-at])
       from-default-value (if from-def
                            from-def
                            (date-util/get-time settings-from-default 15 0 true))
       to-def @(re-frame/subscribe [::dialog-value :def-to])
       to-default-value (if to-def
                          to-def
                          (date-util/get-curr-time 15 0))
       time-range?  @(re-frame/subscribe [::time-range?])
       set-default-value-events [[::update-dialog-values paths/from-key from-default-value]
                                 [::update-dialog-values paths/to-key to-default-value]]]
  [:div {:style {:display :flex
                 :flexDirection :row}}
    [:div {:style {:display :flex
                   :flexDirection :column}}
     [mui/form-group
      [mui/form-control-label
       {:control (reagent/as-element 
                  [mui/switch {:size :small
                               :checked time-range?
                               :onChange #(re-frame/dispatch [::time-range (not time-range?)
                                                              set-default-value-events])}])

        :label (reagent/as-element
                [mui/typography {:style {:font-size "12px"}}
                 "Uhrzeiten?"])
        :label-placement "bottom"}]]] ;[mui/av-timer-icon]}]]])}]]])
   (when time-range?
     [:div {:style {:display :flex
                    :flexDirection :column}}
      [time-field from-default-value paths/from-key "Von"
        [mui/clock-outline-icon {:fontSize :small}]
        "" true
        set-default-value-events]])
   (when time-range?
    [:div {:style {:display :flex
                   :flexDirection :column}}
     [time-field to-default-value paths/to-key "Bis" nil "" true]])
   (when-not time-range?
    [:div {:style {:display :flex
                   :flexDirection :column}}
      [time-field default-value paths/hours-key "Stunden"
       [mui/timelapse-icon {:fontSize :small}]
       "" true]])
   [:div {:style {:display (when time-range? :flex)
                  :flexDirection :column}}
     [time-field default-value paths/pause-key "Pause"
      [mui/timer-icon {:fontSize :small}]
      "Optional" false]]]))

(defn dialog-content []
  [:div {:style {:display :flex
                 :flex-direction :column}}
    [project-row]
    [query-row]
    [issue-row]
    [activity-row]
    [date-row]
    [comment-row]
    [hours-row]])

(re-frame/reg-event-fx
 ::trigger-save
 (fn [{db :db} [_ new-dialog?]]
    (let [is-valid? (dialog-values-valid? db) 
          table-entry (get-in db (conj paths/new-dialog :table-entry))]
      (if new-dialog?
       {:dispatch-later (cond-> []
                          (and is-valid? table-entry)
                          (conj {:ms 0 :dispatch [::update-entry table-entry]})
                          (and is-valid? (not table-entry))
                          (conj 
                           {:ms 0 :dispatch [::force-close]}
                           {:ms 500 :dispatch [::save-entry new-dialog?]}))}

       {:dispatch-n [(when (and is-valid? table-entry)
                      [::update-entry table-entry])
                     (when (and is-valid? (not table-entry))
                      [::save-entry new-dialog?])]}))))

(defn dialog []
  (let [dia-vals @(re-frame/subscribe [::show-dialog?])
        show? (get dia-vals :show? false)
        table-entry (get dia-vals :table-entry)
        dialog-title (if table-entry
                       "Eintrag bearbeiten"
                       "Neuen Eintrag erstellen")
        dialog-values-valid? @(re-frame/subscribe [::dialog-values-valid?])]
     [mui/dialog {:open show?
                     :hideBackdrop true
                     :PaperComponent (reagent/as-component js/ReactDraggable)}
                    ;  :on-close #(re-frame/dispatch [::show-dialog false])}
      [mui/paper
       [mui/dialog-title {:id "create-new-entry-dialog"}
         dialog-title
         [mui/typography {:component "div"
                          :variant "subtitle1"}
             "Die Einträge werden beim Speichern noch nicht gebucht"]]
       [mui/dialog-content {:on-mouse-down #(.stopPropagation %)}
        ;[new-entries/content]]
        [dialog-content]]
       [mui/dialog-actions {:on-mouse-down #(.stopPropagation %)}
         [mui/button {:color "secondary"

                          :on-click #(re-frame/dispatch [::show-dialog false])} ;TODO Reset ui here with already saved entries (revert function needed?)
          "Abbrechen"]

        [mui/button {:color "primary"
                         :disabled (not dialog-values-valid?)
                         :on-click #(if table-entry
                                      (re-frame/dispatch [::update-entry table-entry])
                                      (re-frame/dispatch [::save-entry false]))}
         [mui/tooltip {:title "Speichern und Dialog beenden"
                           :placement "top"}
          [mui/save-icon]]
         "Speichern"]
        (when-not table-entry
         [mui/icon-button {:variant "contained"
                               :size "small"
                               :color "primary"
                               :disabled (not dialog-values-valid?)
                               :on-click (fn []
                                           (re-frame/dispatch [::trigger-save true]))}
            [mui/tooltip {:title "Speichern und weiteren Eintrag erstellen"
                              :placement "top"}
             [mui/add-circle-icon]]])]]]))
