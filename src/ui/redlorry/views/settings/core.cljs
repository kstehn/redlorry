(ns redlorry.views.settings.core
  (:require [re-frame.core :as re-frame]
            [redlorry.data :as data]
            [redlorry.paths.redmine :as redmine-paths]
            [redlorry.paths.views :as views-paths]
            [redlorry.settings :as settings]
            [redlorry.material-ui :as mui]
            [redlorry.api.redmine :as redmine-api]
            [clojure.string :as cstr]
            [redlorry.views.settings.redmine :as redmine-settings]
            [redlorry.views.settings.timetracking :as timetracking-settings]
            [redlorry.views.settings.general :as general-settings]
            [redlorry.views.settings.util :as sutil]))

(re-frame/reg-event-fx
 ::save-settings-node
 (fn [{db :db} [_ settings-node kv-map]]
   (settings/update-setting settings-node kv-map)
   {:db (assoc-in db sutil/settings-save-path (merge
                                               (get-in db sutil/settings-save-path {})
                                               kv-map))
    :dispatch-n [(when (= settings-node redmine-settings/settings-node)
                   [:redlorry.views.starting/init-load])
                 [:redlorry.components.status/show-status-message "Einstellungen gespeichert" :success]]}))

(re-frame/reg-sub
 ::get-settings-for-key
 (fn [db [_ settings-key]]
   (sutil/settings-for-key db settings-key)))

(defn save-settings [db settings-key value]
  (assoc-in db
            (conj sutil/settings-save-path settings-key)
            value))

(re-frame/reg-event-db
 ::settings
 (fn [db [_ settings-key value]]
   (save-settings db settings-key value)))

(def default-settings
  {:api-key ""
   :url ""
   :booking-diagramm true
   :keep-options []
   :default-project ""
   :default-query ""
   :default-activity ""
   :timerange-default false
   :unbooked-entries-warning true
   :default-start-working-at "09:30"
   :autoupdatecheck true})

(re-frame/reg-sub
 ::settings
 (fn [db _]
   (merge default-settings
          (get-in db sutil/settings-save-path
                  {:api-key (get-in db redmine-paths/api-key)
                   :url (get-in db redmine-paths/url)}))))

(defn save-setting
  ([settings-node settings-key]
   (save-setting settings-node settings-key nil))
  ([settings-node settings-key d-value]
   (let [setting-val (get settings-node settings-key d-value)]
     (when (not (nil? setting-val))
       (re-frame/dispatch [::settings settings-key setting-val])))))

(defn settings-loaded [settings]
  (let [{redmine-settings :redmine
         :keys [timetracking general keybindings]} settings]
    (when (and (:url redmine-settings)
               (:api-key redmine-settings))
      (re-frame/dispatch [::redmine-api/set-url (:url redmine-settings)])
      (re-frame/dispatch [::redmine-api/set-api-key (:api-key redmine-settings)])
      (save-setting redmine-settings :url)
      (save-setting redmine-settings :api-key))
    (when keybindings
      (re-frame/dispatch [:redlorry.utils.shortcuts/restore-keybindings keybindings]))
    (save-setting timetracking :timerange-default false)
    (save-setting timetracking :unbooked-entries-warning true)
    (save-setting timetracking :default-project)
    (save-setting timetracking :default-query)
    (save-setting timetracking :default-activity)
    (save-setting timetracking :keep-options)
    (save-setting timetracking :booking-diagramm true)
    (save-setting timetracking :default-start-working-at "09:30")
    (save-setting general :autoupdatecheck true)
    (when (:start-view general)
      (re-frame/dispatch [::settings :start-view (:start-view general)])
      (re-frame/dispatch [:redlorry.views.main-view/switch-view (:start-view general)]))))

(defn content []
  [:div.content
   [general-settings/content]
   [redmine-settings/content]
   [timetracking-settings/content]])

(def view-desc {views-paths/view-id-key :settings
                views-paths/view-label-key "Einstellungen"
                views-paths/view-icon-key [mui/settings-icon {:font-size :small
                                                              :color :disabled}]
                views-paths/view-content-key content})
