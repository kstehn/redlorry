(ns redlorry.views.settings.redmine
  (:require [re-frame.core :as re-frame]
            [reagent.core :as reagent]
            [redlorry.material-ui :as mui]
            [redlorry.views.settings.util :as sutil]))

(def settings-node :redmine)
(def apikey-key :api-key)
(def url-key :url)
(def setting-keys [url-key
                   apikey-key])
(def save-event [::sutil/save-settings settings-node setting-keys [[:redlorry.api.redmine/set-url url-key]
                                                                   [:redlorry.api.redmine/set-api-key apikey-key]]])

(defn get-selection-kv [v]
  (let [val (aget v "target" "value")
        label (aget v "nativeEvent" "target" "textContent")]
    (if (vector? (js->clj val))
      (mapv (fn [v]
              [v label])
            val)
      [label val])))

(defn content []
  (let [url @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key url-key])
        api-key @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key apikey-key])]
    [mui/expansion-panel {:expanded true}
     [mui/expansion-panel-summary {:style {:cursor :default
                                           :min-height "40px"
                                           :height "40px"}}
      [mui/typography ;{:class "settings-header"}
       "Redmine"]]
     [mui/expansion-panel-details
      [:div {:style {:width "100%"}}
       [:div {:style {:display :flex
                      :flexDirection :row}}
        [mui/textfield {:id "urlControl"
                        :label "URL"
                        :InputProps {:style {:font-size "12px"}}
                        :defaultValue (if url
                                        url
                                        "")
                        :on-blur (partial sutil/update-settings-value
                                          save-event
                                          url-key
                                          url)
                        :variant "outlined"
                        :margin "normal"
                        :helper-text "Textfeld verlassen, um Einstellungen zu übernehmen"
                        :fullWidth true}]
        [mui/textfield {:id "apikeyControl"
                        :label "API-Key"
                        :InputProps {:style {:font-size "12px"}}
                        :defaultValue (if api-key
                                        api-key
                                        "")
                        :on-blur (partial sutil/update-settings-value
                                          save-event
                                          apikey-key
                                          api-key)
                        :variant "outlined"
                        :margin "normal"
                        :fullWidth true}]]]]]))
