(ns redlorry.views.settings.general
  (:require [re-frame.core :as re-frame]
            [reagent.core :as reagent]
            [redlorry.material-ui :as mui]
            [redlorry.util :refer [send-ipc]]
            [redlorry.views.settings.util :as sutil]))

(def settings-node :general)
(def autoupdate-key :autoupdatecheck)
(def startview-key :start-view)
(def setting-keys [autoupdate-key
                   startview-key])
(def save-event [::sutil/save-settings settings-node setting-keys])

(defn check-update-button []
  (let [java-available?  @(re-frame/subscribe [:redlorry.utils.system-conditions/java-available?])
        is-mac? @(re-frame/subscribe [:redlorry.utils.system-conditions/is-mac?])]

    [:div
     [mui/button {:variant "outlined"
                  :style {:margin-bottom "5px"}
                  :size "small"
                  :disabled (not (and java-available? (not is-mac?)))
                  :on-click #(re-frame/dispatch [:redlorry.update-ui/run-check-dialog
                                                 (fn [] (send-ipc "check-update"))])}

      [mui/get-app-icon]
      "Auf Update überprüfen"]
     (when (and (not java-available?)
                (not is-mac?))
       [:div {:style {:font-size "12px"
                      :color "red"}}
        "Installiere Java, um die Update-Funktion zu verwenden"])
     (when is-mac?
       [:div {:style {:font-size "12px"
                      :color "red"}}
        "Die Update-Funktion ist auf macOS nicht verfügbar. Eine manuelle Installation ist erforderlich."])]))

(defn autoupdatecheck-switch []
  (let [autoupdatecheck @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key autoupdate-key])]
    [mui/form-group
     [mui/form-control-label
      {:style {:margin-top "5px"}
       :control (reagent/as-element
                 [mui/switch {:checked autoupdatecheck
                              :size :small
                              :disabled (not @(re-frame/subscribe [:redlorry.utils.system-conditions/updatefeature-available?]))
                              :onChange (partial sutil/update-settings-switch
                                                 save-event
                                                 autoupdate-key)}])

       :label (reagent/as-element
               [mui/typography {:style {:font-size "12px"}}
                "Beim Start von RedLorry auf Updates überprüfen?"])}]]))

(defn content []
  (let [possible-views ["Tätigkeitserfassung" "Einstellungen"]
        start-view @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key startview-key])]
    [:div
     [mui/expansion-panel {:expanded true}
      [mui/expansion-panel-summary {:style {:cursor :default
                                            :min-height "40px"
                                            :height "40px"}}
       [mui/typography; {:class "settings-header"}
        "Allgemein"]]
      [mui/expansion-panel-details
       [:div {:style {:width "100%"}}
        [:div {:style {:display :flex
                       :margin-bottom "20px"}}
         [check-update-button]
         [:div {:style {:display :flex
                        :flexDirection :column
                        :width "40px"}}]
         [autoupdatecheck-switch]]
        [mui/select-component
         {:id "start-view-choose"
          :name "Start-Sicht"
          :value (if start-view
                   start-view
                   "")
          :on-change-func (fn [val]
                            (sutil/update-settings-value
                             save-event
                             startview-key
                             start-view
                             val))
          :options (mui/vec->options possible-views)
          :helper-text "Bereich der beim Start von Redlorry angezeigt werden soll"}]]]]]))
