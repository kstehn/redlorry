(ns redlorry.views.settings.timetracking
  (:require [re-frame.core :as re-frame]
            [reagent.core :as reagent]
            [redlorry.material-ui :as mui]
            [redlorry.views.settings.util :as sutil]))


(def settings-node :timetracking)
(def booking-diagramm-key :booking-diagramm)
(def keep-options-key :keep-options)
(def project-key :default-project)
(def query-key :default-query)
(def activity-key :default-activity)
(def timerange-default-key :timerange-default)
(def unbooked-entries-warning-key :unbooked-entries-warning)
(def start-working-key :default-start-working-at)

(def setting-keys [booking-diagramm-key
                   keep-options-key
                   project-key
                   query-key
                   activity-key
                   timerange-default-key
                   unbooked-entries-warning-key
                   start-working-key])
(def save-event [::sutil/save-settings settings-node setting-keys])

(def valid-options [;{:label "Projekt" :key :project}
                    {:label "Query" :key :query}
                    {:label "Buchungsposten" :key :issue}
                    {:label "Aktivität" :key :activity}
                    {:label "Kommentar" :key :comment}
                    {:label "Datum" :key :date}
                    {:label "Stunden" :key :hours}])

(defn get-selection-kv [v]
  (let [val (aget v "target" "value")
        label (aget v "nativeEvent" "target" "textContent")]
    (if (vector? (js->clj val))
      (mapv (fn [v]
              [v label])
            val)
      [label val])))

(defn update-settings [k v]
  (re-frame/dispatch [:redlorry.views.settings.core/settings
                      k v]))

(defn switch [label data-key]
  (let [data @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key data-key])]
    [mui/form-group
     [mui/form-control-label
      {:control (reagent/as-element
                 [mui/switch {:checked data
                              :size :small
                              :onChange (partial sutil/update-settings-switch
                                                 save-event
                                                 data-key)}])
       :label (reagent/as-element
               [mui/typography {:style {:font-size "12px"}}
                label])}]]))

(defn add-entry-keep-values []
  (let [keep-options @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key keep-options-key])]
    [:div {:style {:width "100%"}}
     [mui/select-component
      {:id "keep-add-values-choose"
       :name "Felder übernehmen"
       :value (if keep-options
                (mapv (fn [[v _]]
                        v)
                      keep-options)
                [])
       :multi true
       :render-value (fn [val]
                       (for [v val]
                         (reagent/as-element [mui/chip {:color "primary"
                                                        :size :small
                                                        :variant "outlined"
                                                        :style {:margin-right "5px"}
                                                        :key v :label v}])))
       :on-change-func (fn [val]
                         (sutil/update-settings-kv save-event
                                                   keep-options-key
                                                   (get-selection-kv val)))

       :options (mui/vec->options valid-options)
       :helper-text "Werte, die bei weiterem Eintrag übernommen werden sollen"}]]))

(defn select-default
  ([data-key title label val options helper-text disable?]
   [select-default data-key title label val options helper-text disable? nil])
  ([data-key title label val options helper-text disable? change-func]
   [mui/select-component
    {:id (str "def-" title "choose")
     :name title
     :defaultname label
     :value (if val
              val
              "")
     :on-change-func #(do
                        (let [old-val [label val]
                              new-val (get-selection-kv %)]
                          (when (and (not= old-val new-val)
                                     change-func)
                            (change-func (aget % "target" "value")))
                          (sutil/update-settings-kv
                           save-event
                           data-key
                           old-val
                           new-val)))
     :options options
     ;:helper-text helper-text
     :disabled disable?}]))

(defn timetracking-defaults []
  (let [default-project @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key project-key])
        default-query @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key query-key])
        default-activity @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key activity-key])
        [p-label p-val] (when (vector? default-project)
                          default-project)
        [q-label q-val] (when (vector? default-query)
                          default-query)
        [a-label a-val] (when (vector? default-activity)
                          default-activity)
        p-options @(re-frame/subscribe [:redlorry.views.timetracking.utils/project-options p-label p-val])
        q-options @(re-frame/subscribe [:redlorry.views.timetracking.utils/query-options
                                        (conj (sutil/settings-key-p project-key)
                                              1)
                                        q-label q-val])
        a-options @(re-frame/subscribe [:redlorry.views.timetracking.utils/activity-options a-label a-val])
        no-project? (or (nil? default-project)
                        (empty? (first default-project)))]
    [:div {:style {:width "100%"
                   :display :flex
                   :flexDirection :row}}
     [select-default
      project-key "Default-Projekt"
      p-label p-val
      p-options
      ""
      false
      (fn [pid]
        (re-frame/dispatch [:redlorry.api.redmine/list-issues {:project-id pid}])
        (re-frame/dispatch [:redlorry.api.redmine/list-queries pid]))]
     [:div {:style {:display :flex
                    :flexDirection :column
                    :width "20px"}}]

     [select-default
      query-key "Default-Query"
      q-label q-val
      q-options
      ""
      no-project?]
     [:div {:style {:display :flex
                    :flexDirection :column
                    :width "20px"}}]
     [select-default
      activity-key "Default-Aktivität"
      a-label a-val
      a-options
      ""
      false]]))

(defn default-start-working []
  (let [default-start-working @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key start-working-key])]
    [mui/textfield  {:type "time"
                     :style {:margin-right "30px"
                             :width "50%"}
                     :helperText "Wann beginnt dein Arbeitstag?"
                     :defaultValue default-start-working
                     :label "Start Arbeitstag"
                     ;:fullWidth true
                     :on-blur (partial sutil/update-settings-value
                                       save-event
                                       start-working-key
                                       default-start-working)
                     :inputProps {:step 900
                                  :min 0}
                     :InputProps {:startAdornment
                                  (reagent/as-element [mui/input-adornment {:position :start}
                                                       [mui/clock-outline-icon {:font-size :small}]])}}]))
(defn content []
  [mui/expansion-panel {:expanded true}
   [mui/expansion-panel-summary {:style {:cursor :default
                                         :min-height "40px"
                                         :height "40px"}}
    [mui/typography]
    [mui/typography ;{:class "settings-header"}
     "Zeiterfassung"]]
   [mui/expansion-panel-details
    [:div {:style {:width "100%"}}
     [timetracking-defaults]
     [mui/divider {:style {:margin-top "15px"
                           :margin-bottom "8px"}}]
     [:div {:style {:display :flex
                    :flexDirection :row}}
      [switch "Warnung beim Schließen, wenn ungebuchte Einträge vorhanden sind" unbooked-entries-warning-key]
      [switch "Default: Uhrzeiten eingeben?" timerange-default-key]
      [switch "Diagramm im unteren Bereich anzeigen?" booking-diagramm-key]]
     [mui/divider {:style {:margin-top "8px"}}]
     [:div {:style {:display :flex
                    :flexDirection :row
                    :margin-top "20px"}}
      [default-start-working]
      [add-entry-keep-values]]]]])
