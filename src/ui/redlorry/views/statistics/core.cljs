(ns redlorry.views.statistics.core
  (:require [re-frame.core :as re-frame]
            [redlorry.paths.views :as views-paths]
            [redlorry.material-ui :as mui]))

(defn content []
    [:div.content
     [mui/typography
      "Statistik (TODO)"]])

(def view-desc {views-paths/view-id-key :statistics
                views-paths/view-label-key "Statistik"
                views-paths/view-icon-key [mui/bar-chart-icon {:font-size :small
                                                                  :color :disabled}]
                views-paths/view-content-key content})
