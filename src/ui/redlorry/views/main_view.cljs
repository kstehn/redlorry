(ns redlorry.views.main-view
  (:require [re-frame.core :as re-frame]
            [redlorry.views.timetracking.core :as timetracking]
            [redlorry.views.settings.core :as settings]
            [redlorry.views.statistics.core :as statistics]
            [redlorry.views.shortcut-view :as shortcut-view]
            [redlorry.paths.views :as paths]
            [redlorry.components.core :as components]
            [redlorry.material-ui :as mui]
            [redlorry.config :as config]
            [redlorry.update-ui :as upd-ui]
            [redlorry.themes :as themes]
            [clojure.string :as clj-str]
            [redlorry.util :refer [open-file]]
            [reagent.core :as r]))

(def views (conj []
                 timetracking/view-desc
                ; statistics/view-desc
                 settings/view-desc
                 shortcut-view/view-desc))

;TODO Geht bestimmt noch etwas schicker, evtl. auch nur den Key speichern? (Settings speichert label)
(defn get-content [current-view]
  (get-in (filterv (fn [{id paths/view-id-key
                         label paths/view-label-key}]
                     (or (= current-view label)
                         (= current-view id)))
                   views)
          [0 paths/view-content-key]))

(re-frame/reg-event-db
 ::start-ui
 (fn [db]
   (assoc-in db [:start-ui] true)))

(re-frame/reg-sub
 ::start-ui?
 (fn [db]
   (get-in db [:start-ui] false)))

(re-frame/reg-event-fx
 ::switch-view
 (fn [{db :db} [_ view]]
   {:db (assoc-in db paths/current-view view)}))

(re-frame/reg-sub
 ::current-view
 (fn [db _]
   (get-in db paths/current-view :settings)))

(defn navigation [current-view]
  [mui/drawer {:variant "permanent"
               :anchor "left"
               :PaperProps {:style {:overflow :hidden}}}
   (apply conj
          [mui/mui-list {:style {:padding-top "64px"
                                 :width "52px"}}]
          (mapv (fn [{action-key paths/view-id-key
                      prim-text paths/view-label-key
                      icon paths/view-icon-key
                      display-fn paths/display-fn-key
                      content paths/view-content-key}]
                  [:div
                   [mui/divider]
                   [mui/listitem {:button true
                                  :key action-key
                                  :style {:min-width "0px"}
                                  :on-click #(do
                                               (when content
                                                 (re-frame/dispatch [::switch-view action-key]))
                                               (when display-fn
                                                 (display-fn)))}
                    [mui/tooltip {:title prim-text
                                  :placement :right}
                     [mui/listitem-icon {:style {:min-width "0px"}}
                      icon]]]])
                views))
   [:div {:style {:bottom 10
                  :position :absolute
                  :width "100%"}}
    [:center
     [mui/typography {:style {:font-size 11
                              :color "rgb(51,153,153)"}}
      ; config/app-name
      config/full-version]]]])

(defn app-bar []
  (let [{:keys [status]} @(re-frame/subscribe [:redlorry.api.redmine/ping-status])
        unbooked-count (count @(re-frame/subscribe [:redlorry.views.timetracking.tabs.booking/new-entries]))]
    [mui/appbar {:position "fixed"
                 :style {:backgroundColor "rgb(1,113,113)"
                         :z-index 1201}}
     [mui/toolbar {:disableGutters true}
      [mui/svg-icon {:style {:padding-top "1px"
                             :padding-bottom "1px"
                             :margin-left "10px"
                             :padding-left "8px"
                             :padding-right "5px"
                             :display :inline
                             :background-color "white"
                             :border-radius "20px"
                             :cursor :pointer
                             :color (if status
                                      "rgb(0, 209, 102)"
                                      "rgb(240, 22, 62)")}
                     :titleAccess (if status
                                    "Redmine erreichbar"
                                    "Verbindung zu Redmine getrennt")
                     :on-click #(let [url @(re-frame/subscribe [:redlorry.api.redmine/url])]
                                  (when (and (not= url "")
                                             (clj-str/includes? url "http"))
                                    (open-file @(re-frame/subscribe [:redlorry.api.redmine/url]))))
                     :font-size :default}
       [mui/public-icon]]
      [mui/typography {;:useNextVariants true
                       :variant "h5"
                       :style {:color "white"
                               :padding-left "10px"}}
       config/app-name]
      (when (> unbooked-count 0)
        [mui/chip {;:color "primary"
                   :size :small
                   :avatar (r/as-element [mui/avatar {:style {:backgroundColor "white"}}
                                          [mui/style-icon {:font-size :small}]])
                   :style {:margin-left "180px"
                           :margin-top "10px"
                           :opacity "0.7"}
                   :label (if (= unbooked-count 1)
                            (str unbooked-count " ungebuchter Eintrag")
                            (str unbooked-count " ungebuchte Einträge"))}])
      [mui/card {:style {:position "absolute"
                         :right 10
                         :width "120px"
                         :height "50px"}}
       [mui/card-media {:image "img/logo.svg"
                        :style {:height "46px"
                                :width "100px"
                                :margin-left "10px"
                                :margin-top "2px"
                                :opacity "0.7"}}]]]]))

(defonce theme (mui/createMuiTheme
                (clj->js {:typography
                          {:useNextVariants true}})))
                 ;themes/d42-theme TODO noch nicht so schick))

(defn starting-view []
  [:div
   [:div {:id "redlorry_bg"}]
   [:div {:id "redlorry_bg2"}]
   [:div {:id "redlorry_left_accent"}]
   [mui/paper {:style {:position :absolute
                       :bottom 200
                       :width "100%"}
               :align "center"}
    (if @(re-frame/subscribe [:redlorry.update-ui/update-checking-running?])
      [upd-ui/update-view]
      [components/loading "Redlorry wird gestartet..."])]])

(defn redlorry []
  [:div.main
   [components/confirm-dialog]
   [components/status]
   [shortcut-view/dialog]
   (if @(re-frame/subscribe [::start-ui?])
     (let [current-view @(re-frame/subscribe [::current-view])
           view-component (get-content current-view)]
       [mui/mui-theme-provider {:theme theme}
        [:div  {:style {:padding-top "64px"
                        :padding-left "40px"}}
         [app-bar]
         [navigation current-view]
         [:div {:id "redlorry_bg"}]
         [:div {:id "redlorry_bg2"}]
         [:div {:id "redlorry_left_accent"}]
         (when view-component
           [view-component])]])
     [starting-view])])
