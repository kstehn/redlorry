(ns redlorry.utils.xlsx-writer
  (:require exceljs
            fs
            [redlorry.data :refer [write-file-sync]]
            [re-frame.core :as re-frame]
            [redlorry.util :refer [save-dialog open-dialog open-file]]))


(def default-author "RedLorry")
(def default-title "RedLorry Export")

(def ws-props (clj->js {:pageSetup  {:fitToPage true
                                     :orientation :landscape
                                     :fitToHeight 0
                                     :paperSize 9}
                        :properties {:defaultRowHeight 20}
                        :views [{:state "frozen" :ySplit 1}]}))

(def default-headerstyle {:font {:size 11 :bold true
                                 :color {:argb "FFFFFFFF"}}
                          :alignment {:vertical "middle"
                                      :horizontal "center"}
                          :fill {:type "pattern"
                                 :pattern "solid"
                                 :fgColor {:argb "FF359797"}
                                 :bgColor {:argb "FF359797"}}
                          :border {:top {:style "thin" :color {:argb "FF404040"}}
                                   :left {:style "thin" :color {:argb "FFFFFFFF"}}
                                   :bottom {:style "thin" :color {:argb "FF404040"}}
                                   :right {:style "thin" :color {:argb "FFFFFFFF"}}}})

(def default-row-style {:font {:size 10
                               :color {:argb "FF404040"}}})

(def default-sum-style {:font {:size 12 :bold true
                               :color {:argb "FFFFFFFF"}}
                        :alignment {:vertical "middle"
                                    :horizontal "center"}
                        :fill {:type "pattern"
                               :pattern "solid"
                               :fgColor {:argb "FFF50057"}
                               :bgColor {:argb "FFF50057"}}
                        :border {:top {:style "thin" :color {:argb "FF404040"}}
                                 :left {:style "thin" :color {:argb "FFFFFFFF"}}
                                 :bottom {:style "thin" :color {:argb "FF404040"}}
                                 :right {:style "thin" :color {:argb "FFFFFFFF"}}}})

(def default-row-borders  {:top {:style "thin" :color {:argb "FF404040"}}
                           :left {:style "thin" :color {:argb "FF404040"}}
                           :bottom {:style "thin" :color {:argb "FF404040"}}
                           :right {:style "thin" :color {:argb "FF404040"}}})

(def testdata
  [{:spent_on "10.02.2010"
    :hours 8.2
    :project "test"
    :issue "testbp"
    :activity  "Dev"
    :comments "Kommentar"}
   {:spent_on "13.08.2012"
    :hours 1
    :project "test2"
    :issue "testbp2"
    :activity  "Dev2"
    :comments "Kommentar2"}])

(defn column-entry [{:keys [k label w v-align h-align wrap-text]}]
  {:header label
   :filterButton true
   :key k
   :width w
   :wrap-text (or wrap-text false)
   :style (merge
           default-row-style
           {:alignment {:vertical (or v-align "middle")
                        :horizontal (or h-align "center")}})})

(defn col-num->char [num]
  ;only works from A:Z
  (char (+ num 65)))

(defn gen-header [ws header]
  (let [columns (mapv column-entry header)]
    (aset ws "columns" (clj->js columns))))

(defn apply-header-style [ws]
  (let [columns (aget ws "columnCount")]
    (doseq [c (range 0 columns)]
      (let [col (col-num->char c)
            cell (str col "1")]
        (aset (.getCell ws cell) "style"
              (cond
                (= c 0)
                (clj->js (assoc-in default-headerstyle [:border :left :color :argb] "FF404040"))
                (= c (- columns 1))
                (clj->js (assoc-in default-headerstyle [:border :right :color :argb] "FF404040"))
                :else
                (clj->js default-headerstyle)))))))

(defn apply-row-style [ws]
  (let [columns (aget ws "columnCount")
        rows (aget ws "rowCount")]
    (doseq [c (range 0 columns)]
      (doseq [r (range 2 (+ 1 rows))]
        (let [col (col-num->char c)
              cell (str col r)]
          (aset (.getCell ws cell)
                "style" "border"
                (clj->js default-row-borders)))))))

(defn create-workbook []
  (let [wb (js/EXCELJS.Workbook.)]
    (aset wb "creator" default-author)
    (aset wb "title" default-title)
    (aset wb "created" (js/Date.))
    wb))

(defn add-row [ws rowdata]
  (.addRow ws (clj->js rowdata)))

(defn sum-by-k [k data]
  (->> data
       (map k)
       (reduce +)))

(defn add-sum-row [ws header data]
  (let [rows (aget ws "rowCount")
        columns (aget ws "columnCount")
        sum-row (+ 1 rows)]
    (doseq [c (range 0 columns)]
      (let [{:keys [k sum-by]} (get header c)]
        (let [col (col-num->char c)
              cell (str col sum-row)]
          (when (and k sum-by)
            (let [val (cond (= sum-by :sum)
                            {:formula (str "SUM(" col "2:" col rows ")")
                             :result (sum-by-k k data)}
                            (= sum-by :count)
                            {:formula (str "ROWS(" col "2:" col rows ")")
                             :result (- rows 1)})]
              (aset (.getCell ws cell)
                    "value"
                    (clj->js val))))
          (aset (.getCell ws cell)
                "style"
                (clj->js
                 (cond (= c 0)
                       (clj->js (assoc-in default-sum-style [:border :left :color :argb] "FF404040"))
                       (= c (- columns 1))
                       (clj->js (assoc-in default-sum-style [:border :right :color :argb] "FF404040"))
                       :else
                       default-sum-style))))))))

(defn type-save-dialog [exp-type default-filename]
  (let [filter-entry (case exp-type
                       :excel {:name "Excelsheet file" :extensions ["xlsx"] :booktype "xlsx"}
                       :csv {:name "csv" :extensions ["csv"] :booktype "csv"}
                       :html {:name "html" :extensions ["html"] :booktype "html"})
        file  (save-dialog {:title "Einträge speichern"
                            :buttonLabel "Speichern"
                            :defaultPath (str (or default-filename
                                                  "unnamed")
                                              "."
                                              (get filter-entry :booktype))
                            :filters [(select-keys filter-entry #{:name :extensions})]})]
    {:file file
     :booktype (get filter-entry :booktype)}))

(defn export [{:keys [data header sheetname export-type open-file? cancel-event fail-event success-event default-filename]}]
  (let [{:keys [file booktype]} (type-save-dialog export-type default-filename)]
    (if (and file booktype data)
      (let [wb (create-workbook)
            sheet (or sheetname "export")
            ws (.addWorksheet wb sheet ws-props)
            exp (if (= export-type :excel)
                  "xlsx"
                  "csv")]
        (gen-header ws header)
        (apply-header-style ws)
        (doseq [d data]
          (add-row ws d))
        (apply-row-style ws)
        (add-sum-row ws header data)
        (-> (.then (.writeBuffer (aget wb exp))
                   (fn [buffer]
                     (write-file-sync (js/Buffer.from (js/Uint8Array. buffer))
                                      file)
                     (when open-file?
                       (open-file file))
                     (when success-event
                       (re-frame/dispatch success-event))))

            (.catch (fn [error]
                      (js/console.log "ERR" error)
                      (when fail-event
                        (re-frame/dispatch fail-event))))))
      (when cancel-event
        (re-frame/dispatch cancel-event)))))