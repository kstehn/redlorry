(ns redlorry.utils.shortcuts
  (:require cljsjs.mousetrap-plugins
            [reagent.core :as r]
            [clojure.string :as clj-str]
            [re-frame.core :as re-frame]))

(defn new-entry-trigger-fn []
  (let [view @(re-frame/subscribe [:redlorry.views.main-view/current-view])]
    (when (or (= :timetracking view)
              (= "Tätigkeitserfassung" view))
      (re-frame/dispatch [:redlorry.views.timetracking.entry-dialog/show-dialog true]))))

(defn select-all-trigger-fn [e]
 (when (not= (aget e "target" "id") 
             "entry-dialog-comment") 
  (let [view @(re-frame/subscribe [:redlorry.views.main-view/current-view])
        timeview @(re-frame/subscribe [:redlorry.views.timetracking.core/current-timeview])]
    (when (and
           (= timeview 0)
           (or (= :timetracking view)
               (= "Tätigkeitserfassung" view)))
      (re-frame/dispatch [:redlorry.views.timetracking.tabs.booking/select-all])
      false))))

(defn book-entries-trigger-fn []
  (let [view @(re-frame/subscribe [:redlorry.views.main-view/current-view])
        timeview @(re-frame/subscribe [:redlorry.views.timetracking.core/current-timeview])
        some-selected? (< 0 (count @(re-frame/subscribe [:redlorry.views.timetracking.tabs.booking/selected-rows])))]
    (when (and
           some-selected?
           (= timeview 0)
           (or (= :timetracking view)
               (= "Tätigkeitserfassung" view)))
      (re-frame/dispatch [:redlorry.views.timetracking.tabs.booking/show-book-dialog]))
    false))

(defn delete-entries-trigger-fn []
  (let [view @(re-frame/subscribe [:redlorry.views.main-view/current-view])
        timeview @(re-frame/subscribe [:redlorry.views.timetracking.core/current-timeview])
        some-selected? (< 0 (count @(re-frame/subscribe [:redlorry.views.timetracking.tabs.booking/selected-rows])))]
    (when (and
           some-selected?
           (= timeview 0)
           (or (= :timetracking view)
               (= "Tätigkeitserfassung" view)))
      (re-frame/dispatch [:redlorry.views.timetracking.tabs.booking/show-delete-dialog]))
    false))

(defn save-entry-trigger-fn [new-dialog?]
  (let [view @(re-frame/subscribe [:redlorry.views.main-view/current-view])
        dialog-show? (get @(re-frame/subscribe [:redlorry.views.timetracking.entry-dialog/show-dialog?])
                          :show? false)]
    (when (and
           dialog-show?
           (or (= :timetracking view)
               (= "Tätigkeitserfassung" view)))
      (re-frame/dispatch [:redlorry.views.timetracking.entry-dialog/trigger-save new-dialog?]))
    false))

(defn switch-to-timetracking-trigger-fn []
  (let [view @(re-frame/subscribe [:redlorry.views.main-view/current-view])]
    (when-not (or (= :timetracking view)
                  (= "Tätigkeitserfassung" view))
      (re-frame/dispatch [:redlorry.views.main-view/switch-view :timetracking]))
    false))

(defn switch-to-settings-trigger-fn []
  (let [view @(re-frame/subscribe [:redlorry.views.main-view/current-view])]
    (when-not (or (= :settings view)
                  (= "Einstellungen" view))
      (re-frame/dispatch [:redlorry.views.main-view/switch-view :settings]))
    false))

(defn open-keybinds-trigger-fn []
  (when-not  @(re-frame/subscribe [:redlorry.views.shortcut-view/show-dialog?])
    (re-frame/dispatch [:redlorry.views.shortcut-view/show-dialog true]))
  false)

(defn cancel-entrydialog-trigger-fn []
  (let [view @(re-frame/subscribe [:redlorry.views.main-view/current-view])
        dialog-show? (get @(re-frame/subscribe [:redlorry.views.timetracking.entry-dialog/show-dialog?])
                          :show? false)]
    (when (and
           dialog-show?
           (or (= :timetracking view)
               (= "Tätigkeitserfassung" view)))
      (re-frame/dispatch [:redlorry.views.timetracking.entry-dialog/show-dialog false]))
    false))

(defn export-excel-trigger-fn [export-type]
  (let [view @(re-frame/subscribe [:redlorry.views.main-view/current-view])]
    (when (or (= :timetracking view)
              (= "Tätigkeitserfassung" view))
      (re-frame/dispatch [:redlorry.views.timetracking.core/export export-type]))
    false))

(def default-shortcuts
  {:new-entry {:bind "ctrl+n"
               :label "Neuer Eintrag"
               :section :entry-dialog
               :order 0
               :trigger-fn new-entry-trigger-fn}
   :select-all {:bind "ctrl+a"
                :label "Alle Einträge de/selektieren"
                :section :booking
                :order 40
                :trigger-fn select-all-trigger-fn}
   :book-entries {:bind "ctrl+b"
                  :label "Markierte Einträge buchen"
                  :section :booking
                  :order 50
                  :trigger-fn book-entries-trigger-fn}
   :delete-entries {:bind "ctrl+d"
                    :label "Markierte Einträge löschen"
                    :section :booking
                    :order 51
                    :trigger-fn delete-entries-trigger-fn}
   :save-entry {:bind "ctrl+s"
                :label "Eintrag speichern"
                :section :entry-dialog
                :order 10
                :trigger-fn #(save-entry-trigger-fn false)}
   :save-create-entry {:bind "ctrl+shift+s"
                       :label "Eintrag speichern und weiteren erstellen"
                       :section :entry-dialog
                       :order 20
                       :trigger-fn #(save-entry-trigger-fn true)}
   :cancel-entry-dialog {:bind "ctrl+q"
                         :label "Eintrag Dialog schließen"
                         :section :entry-dialog
                         :order 30
                         :trigger-fn cancel-entrydialog-trigger-fn}
   :switch-to-timetracking {:bind "ctrl+1"
                            :label "Wechsel zu Zeiterfassung"
                            :section :general
                            :order 70
                            :trigger-fn switch-to-timetracking-trigger-fn}
   :switch-to-settings {:bind "ctrl+2"
                        :label "Wechsel zu Einstellungen"
                        :section :general
                        :order 80
                        :trigger-fn switch-to-settings-trigger-fn}
   :open-keybindings {:bind "ctrl+3"
                      :label "Öffne Shortcuts"
                      :section :general
                      :order 90
                      :trigger-fn open-keybinds-trigger-fn}
   :export-excel {:bind "ctrl+e e"
                  :label "Excel Export"
                  :section :export
                  :order 100
                  :trigger-fn #(export-excel-trigger-fn :excel)}
   :export-csv {:bind "ctrl+e c"
                :label "CSV Export"
                :section :export
                :order 110
                :trigger-fn #(export-excel-trigger-fn :csv)}})

(def shortcuts (r/atom default-shortcuts))

(defn get-shortcuts []
  @shortcuts)

(defn extract-bindings [shortcut-map]
  (reduce (fn [res [k {:keys [bind]}]]
            (assoc res k bind))
          {}
          shortcut-map))

(defn get-default-bindings []
  (extract-bindings default-shortcuts))

(defn register-all []
  (.reset js/Mousetrap)
  (aset js/Mousetrap.prototype "stopCallback" (fn [e element combo]
                                                false))
  (doseq [{:keys [bind trigger-fn]} (vals @shortcuts)]
    (js/Mousetrap.bind bind trigger-fn)))

(defn stop []
  (.reset js/Mousetrap))

(defn resume []
  (register-all))

(defn record-shortcut [callback]
  (js/Mousetrap.record
   (fn [sequence]
     (callback (clj-str/join " " sequence)))))

(defn is-same-binding? [shortcut-id new-bind]
  (= new-bind (get-in @shortcuts [shortcut-id :bind])))

(defn binding-exist?
  ([check-bind]
   (binding-exist? check-bind nil))
  ([check-bind exclude-ids]
   (some #(and
           (or (nil? exclude-ids)
               (not (some (fn [id]
                            (= (get % 0) id))
                          exclude-ids)))
           (= check-bind (get-in % [1 :bind])))
         @shortcuts)))

(defn set-binding [shortcut-id new-binding]
  (let [already-exist? (binding-exist? new-binding)]
    (when-not already-exist?
      (swap! shortcuts assoc-in [shortcut-id :bind] new-binding))))

(defn set-bindings [bindings]
  (doseq [[id bind] bindings]
    (set-binding id bind)))

(re-frame/reg-event-db
 ::restore-keybindings
 (fn [db [_ bindings]]
   (set-bindings bindings)
   (register-all)
   db))

(defn save-bindings [bindings]
  (set-bindings bindings)
  (re-frame/dispatch [:redlorry.views.settings.core/save-settings-node
                      :keybindings
                      bindings]))
