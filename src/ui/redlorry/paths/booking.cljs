(ns redlorry.paths.booking
  (:require [redlorry.paths.views :as views-paths]))


;==== New Timeentry ====
(def timeentries-new :new)
(def new-timeentries-section
  (conj views-paths/time-entries timeentries-new))

;==== New Entry Rows ====
(def new-entry-rows (conj new-timeentries-section :entry-rows))
(defn new-entry-row [entry-id]
  (conj new-entry-rows entry-id))

;==== Booking Timeentries ====
(def timeentries-tobook :to-book)
(def timeentries-bookall :book-all)

(def select-all-new-entries
  (conj new-timeentries-section timeentries-bookall))

(def to-book-entries
 (conj new-timeentries-section timeentries-tobook))
