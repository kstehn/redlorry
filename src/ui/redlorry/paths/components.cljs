(ns redlorry.paths.components
    (:require [redlorry.paths.views :as views-paths]))

;==== Confirm dialog Timeentries ====
(def dialog-key :dialog)
(def confirm (conj views-paths/time-entries :confirm))
(def dialog (conj confirm dialog-key))
(def dialog-show? (conj dialog :show?))

;==== Status ====
(def status-root :status)
(def status [views-paths/root-key status-root])
(def status-message-key :status-message)
(def status-message (conj status status-message-key))
(def status-exiting (conj status-message :exiting))

;==== Table Timetracking ====
(def tablesorting (conj views-paths/time-entries :tablesorting))
(def tablesorting-direction (conj tablesorting :direction))
(def tablescroll (conj views-paths/time-entries :tablescroll))
