(ns redlorry.paths.views)

(def root-key :views)

;==== Timetracking ====
(def time-tracking-root :timetracking)
(def time-tracking [root-key time-tracking-root])

;==== Timeentries ====
(def time-entries-root :timeentries)
(def time-entries [root-key time-tracking-root time-entries-root])

;==== App Navigation ====
(def navigation-root :navigation)
(def show-navigation-key :show?)
(def current-view-key :current-view)
(def navigation [navigation-root show-navigation-key])
(def current-view [navigation-root current-view-key])

;==== View Description ====
(def view-id-key :id)
(def view-label-key :label)
(def view-icon-key :icon)
(def view-content-key :content)
(def display-fn-key :display-fn)
