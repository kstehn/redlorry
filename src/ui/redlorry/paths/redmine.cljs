(ns redlorry.paths.redmine)

;====== Paths =======
(def root :redmine)
(def api-key-key :api-key)
(def url-key :url)

;===== Connection =====
(def api-key [root api-key-key])
(def url [root url-key])
(def interval [root :interval-id])
(def connection-status [root :con-status])
(def user-infos [root :user-info])

;===== Data ======
(def projects [root :projects])
(def time-entry-activities [root :time-entry-activities])
(def queries [root :queries])

(def time-entries-root [root :time-entries])
(defn time-entries [period]
  (conj time-entries-root period))

(def issues-root [root :issues])
(defn issues [query-id]
  (conj issues-root query-id))

