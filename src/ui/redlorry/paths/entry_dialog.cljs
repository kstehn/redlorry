(ns redlorry.paths.entry-dialog
  (:require [redlorry.paths.booking :as booking-paths]
            [redlorry.paths.components :as comp-paths]))

(def dialog-key comp-paths/dialog-key)
(def new-timeentries booking-paths/new-timeentries-section)

;==== Entry Dialog ====
(def project-key :project)
(def query-key :query)
(def issue-key :issue)
(def activity-key :activity)
(def date-key :date)
(def comment-key :comment)
(def hours-key :hours)
(def from-key :from)
(def to-key :to)
(def pause-key :pause-key)
(def new-dialog (conj booking-paths/new-timeentries-section comp-paths/dialog-key))
(def values-root (conj new-dialog :values))
(def table-entry (conj new-dialog :table-entry))

(def project-val (conj values-root project-key))
(def query-val (conj values-root query-key))
(defn dialog-value [dialog-key]
  (conj values-root dialog-key))

;==== New Entry Rows ====
(def new-entry-rows booking-paths/new-entry-rows)
(def new-entry-row booking-paths/new-entry-row)
