(ns redlorry.components.table
  (:require [re-frame.core :as re-frame]
            [redlorry.material-ui :as mui]
            [redlorry.utils.date :as u-date]
            [redlorry.utils.things :refer [get-unique-dom-key sum-by-key]]
            [redlorry.paths.components :as paths]))

(defn foot-icon [{:keys [label icon title style]}]
  [mui/typography {:component "div"
                   :color :secondary
                   :style style}
   [mui/svg-icon {:font-size :small
                  :color "action"
                  :titleAccess title
                  :style {:position :absolute
                          :top "10px"}}
    [icon]]
   [:font {:style {:margin-left "25px"}}
    label]])

(defn table-footer [entries show-month-average]
  (let [dom-key (get-unique-dom-key)
        additional-style {:display :inline
                          :margin-left "20px"
                          :font-size "13px"}
        hours-sum (sum-by-key entries [:hours :value])
        hours-sum-str (.toFixed hours-sum 2)
        dates (group-by #(get-in % [:spent_on :sort-value]) entries)
        month-count (if show-month-average
                      (count (set (mapv #(u-date/to-month %) (keys dates))))
                      1)
        dates-count (count (keys dates))
        average-hours-per-day (if (not= 0 dates-count)
                                (.toFixed (/ hours-sum dates-count)
                                          2)
                                0)
        average-hours-per-month (if (and show-month-average (not= 0 dates-count))
                                  (.toFixed (/ hours-sum month-count)
                                            2)
                                  0)
        entries-count (count entries)]
    [mui/paper {:key (str dom-key "table-footer")
                :square true
                :style {:width "100%"
                        :height "40px"
                        :position :relative}}
     [mui/svg-icon {:style {:padding-top "2px"
                            :padding-left "10px"
                            :display :inline}
                    :font-size :large
                    :color "secondary"}
      [mui/functions-icon]]
     [:div {:style {:display :inline-block
                    :position :absolute
                    :height "40px"
                    :padding-top "10px"}}
      [foot-icon
       {:label (str entries-count (if (= 1 entries-count)
                                    " Eintrag"
                                    " Einträge"))
        :icon mui/style-icon
        :title "Anzahl Einträge"
        :style additional-style}]
      [foot-icon
       {:label (str dates-count (if (= 1 dates-count)
                                  " Tag"
                                  " Tage"))
        :icon mui/event-available-icon
        :title "Anzahl Tage"
        :style additional-style}]
      [foot-icon
       {:label (str hours-sum-str " Stunden")
        :icon mui/timelapse-icon
        :title "Summe Stunden"
        :style additional-style}]
      [foot-icon
       {:label (str average-hours-per-day " Stunden")
        :icon mui/timer-icon
        :title "Stunden pro Tag (Durchschnitt)"
        :style additional-style}]
      (when show-month-average
        [foot-icon
         {:label (str average-hours-per-month " Stunden")
          :icon mui/shutter-speed-icon
          :title "Stunden pro Monat (Durchschnitt)"
          :style additional-style}])]]))

(defn entry-row [entry]
  (reduce
   (fn [parent [_ {:keys [value type style]}]]
     (conj parent
           [mui/table-cell {:key (str (get-unique-dom-key) "value" value type)
                            :style style}

            value]))
   [mui/table-row {:key (str "trow" (get-unique-dom-key))
                   :style {:height "38px"}}]
   (dissoc entry :id :source-entry)))

(re-frame/reg-event-db
 ::sort-by
 (fn [db [_ by]]
   (let [direction (get-in db paths/tablesorting-direction :desc)]
     (assoc-in db paths/tablesorting {:direction (if (= direction
                                                        :desc)
                                                   :asc
                                                   :desc)
                                      :by by}))))

(re-frame/reg-sub
 ::sort-by
 (fn [db]
   (get-in db paths/tablesorting {:direction :desc
                                  :by :spent_on})))

(defn table-head [captions sorting]
  (let [dom-key (get-unique-dom-key)]
    [mui/table-head
     (apply conj
            [mui/table-row {:key (str dom-key "thead")
                            :style {:height "40px"}}]
            (mapv (fn [{:keys [data-key label style]}]
                    (let [cell-dom-key (str dom-key
                                            "thead-cell"
                                            label)
                          cell-map {:key cell-dom-key
                                    :style style}]
                      [mui/table-cell (assoc cell-map
                                             :sortDirection (if (= (:by sorting)
                                                                   data-key)
                                                              (name (:direction sorting))
                                                              false))
                       [mui/table-sort-label {:active (= (:by sorting)
                                                         data-key)
                                              :color :primary
                                              :size :small
                                              :direction (name (:direction sorting))
                                              :on-click #(re-frame/dispatch [::sort-by data-key])}
                        label]]))
                  captions))]))

(defn component [{:keys [captions entries show-month-average]}]
  (let [sorting @(re-frame/subscribe [::sort-by])
        entries (if sorting
                  (vec (sort-by #(get-in % [(:by sorting) :sort-value])
                                (if (= (:direction sorting) :desc)
                                  >
                                  <)
                                entries))
                  (vec entries))]

    [:div {:style {:padding-top "10px"}}
     [mui/table
      [table-head captions sorting]
      [mui/table-body {:style {:display :none}}
       [mui/table-row]]]
     (if (or (nil? entries)
             (empty? entries))
       [:div]
       [:div {:style {:max-height "300px"
                      :overflow :auto
                      :position :relative}}
        [mui/table
         (reduce (fn [parent entry]
                   (conj parent (entry-row entry)))
                 [mui/table-body]
                 entries)]])
     [table-footer entries show-month-average]]))
