(ns redlorry.components.core
  (:require [redlorry.components.charts :as charts]
            [redlorry.components.confirm-dialog :as confirm-dialog]
            [redlorry.components.loading :as loading]
            [redlorry.components.status :as status]
            [redlorry.components.tabs :as tabs]
            [redlorry.components.table :as table]))

(def chart charts/component)

(def confirm-dialog confirm-dialog/component)

(def loading loading/component)

(def status status/component)

(def table table/component)

(def tabs tabs/component)
