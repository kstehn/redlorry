(ns redlorry.components.charts
 (:require [reagent.core :as reagent]
           [cljsjs.chartjs]
           [goog.object :as g]))

(defonce instances (atom {}))

(defn line-chart [id {:keys [labels data chart-options]
                      :or {chart-options {:legend {:display false}
                                          :scales {:yAxes [{:ticks {:min 0
                                                                    :autoSkip true}}]}
                                          :elements {:line {:tension 0}
                                                     :point {:hoverRadius 0}}
                                          :tooltips {:enabled false}
                                          :animation {:duration 0}
                                          :responsiveAnimationDuration 0}}}]
  (let [context (.getContext (.getElementById js/document id) "2d")
        chart-data {:type :line
                    :redraw true
                    :options chart-options
                    :data {:labels labels
                           :datasets [{:data data
                                       :borderColor "#339999"
                                       :fill false
                                       :pointRadius 0}]}}]
    (swap! instances assoc id (js/Chart. context (clj->js chart-data)))))

(defn bar-chart [id {:keys [labels data dataset-label chart-options]
                     :or {chart-options {:legend {:display false}
                                         :scales {:yAxes [{:ticks {:min 0}}]}}}}]
  (let [context (.getContext (.getElementById js/document id) "2d")
        chart-data {:redraw true
                    :options chart-options
                    :data {:labels labels
                           :datasets [{:data data
                                       :label dataset-label
                                       :backgroundColor "#339999"
                                       :hoverBackgroundColor "#ff6e00"}]}}]

    (swap! instances assoc id (js/Chart.Bar. context (clj->js chart-data)))))

(defn update-chart [id {:keys [data labels]}]
 (let [chart-instance (get @instances id)
       chart-data (aget chart-instance 
                        "data" 
                        "datasets" 
                        0)]
    (when chart-instance
      (g/set (g/get chart-instance "data")
             "labels"
             (clj->js labels))
      (g/set chart-data
             "data"
             (clj->js data))
      (.update chart-instance))))

(defn clean-instance [id]
  (let [chart-instance (get @instances id)]
    (when chart-instance
     (.destroy chart-instance)
     (swap! instances dissoc id))))

(defn component [{:keys [id style type]
                  :or {id (str "chart-" (random-uuid))
                       style {:width "100%"
                              :height "150px"
                              :margin-top "20px"
                              :background "rgba(51,153,153,0.05)"}}
                  :as props}]
     (reagent/create-class
      {:component-did-mount
       #(case type
          :bar (bar-chart id props)
          :line (line-chart id props)
          (js/console.warn "Not supported chart-type: " type))
       :component-did-update (fn [e]
                               (let [[_ props] (aget e "props" "argv")]
                                (update-chart id props)))
       :component-will-unmount #(clean-instance id)
       :reagent-render (fn []
                         [:canvas {:key id
                                   :id id
                                   :style style}])}))
