(ns redlorry.components.confirm-dialog
 (:require [redlorry.material-ui :as mui]
           [re-frame.core :as re-frame]
           [redlorry.paths.components :as paths]))


(re-frame/reg-event-db
 ::show-dialog
 (fn [db [_ flag fire-event dialog-title dialog-content-text abort-event props]]
  (if flag
    ;TODO die anderen Parameter nach props verlagern + an den genutzen Stellen nachziehen
    (assoc-in db
              paths/dialog
              (assoc props
                     :show? flag
                     :fire-event fire-event
                     :dialog-title dialog-title
                     :dialog-content-text dialog-content-text
                     :abort-event abort-event))
    (update-in db
               paths/confirm
               dissoc
               paths/dialog-key))))

(re-frame/reg-sub
 ::show-dialog?
 (fn [db]
   (get-in db paths/dialog)))

(re-frame/reg-event-db
 ::exit-dialog
 (fn [db]
   (assoc-in db paths/dialog-show? false)))

(re-frame/reg-event-fx
 ::confirmed
 (fn [db [_ fire-event]]
  {:dispatch-n [fire-event
                [::exit-dialog]]}))

(defn component []
  (let [dia-vals @(re-frame/subscribe [::show-dialog?])
        show? (get dia-vals :show? false)
        fire-event (get dia-vals :fire-event [])
        abort-event (get dia-vals :abort-event)
        dialog-title (get dia-vals :dialog-title "")
        dialog-content-text (get dia-vals :dialog-content-text "")
        {:keys [confirm-label abortlabel confirm-deactivated
                abort-button confirm-button]}
        dia-vals]
     [mui/dialog {:open show?
                   :on-exited #(re-frame/dispatch [::show-dialog false])}
      [mui/dialog-title {:id "confirm-dialog"}
        dialog-title]
      [mui/dialog-content
       (if (string? dialog-content-text)
        [ mui/dialog-content-text {:variant "subtitle2"}
         dialog-content-text]
        dialog-content-text)]
      [mui/dialog-actions
       (when (not= abort-button false)
        [mui/button {:color "primary"
                      :on-click #(do
                                  (when abort-event
                                    (re-frame/dispatch abort-event))
                                  (re-frame/dispatch [::exit-dialog]))}
 
          (if abortlabel
            abortlabel
            "Nein")])
       (when (not= confirm-button false)
        [mui/button {:color "primary"
                      :disabled (if confirm-deactivated
                                  (confirm-deactivated)
                                  false)
                      :on-click #(re-frame/dispatch [::confirmed fire-event])}
          (if confirm-label
            confirm-label
            "Ja")])]]))
