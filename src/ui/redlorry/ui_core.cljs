(ns redlorry.ui-core
  "Houses application state and defines actions and the main application component"
  (:require [reagent.core :as reagent]
            [redlorry.utils.shortcuts :as shortcuts]
            [re-frame.core :as re-frame]
            [redlorry.util :refer [on-ipc get-url send-ipc destroy-current]]
            [redlorry.update-ui :as upd-ui]
            [redlorry.worker :as worker]
            [redlorry.utils.date :as u-date]
            [redlorry.config :as config]
            [redlorry.views.main-view :as main-view]
            [redlorry.views.starting :as starting]
            [redlorry.utils.xlsx-writer]
            [redlorry.utils.system-conditions]
            [cljsjs.react-day-picker]
            [cljsjs.react-draggable]
            [moment :as moment]
            [cljsjs.moment.locale.de]))

(on-ipc "system-conditions" (fn [_ res] (re-frame/dispatch [::starting/system-conditions res])))
(on-ipc "settings-loaded" starting/settings-loaded)
(on-ipc "done-reading-unbooked-entries" starting/unbooked-entries-loaded)
(on-ipc "update-available" upd-ui/update-available)
(on-ipc "update-install-error" upd-ui/update-failed)
(on-ipc "currversion-is-newest" upd-ui/currversion-is-newest)

(re-frame/reg-event-db
 ::initialize-db
 (fn  [_ _]
   {}))

(re-frame/reg-event-db
 ::final-close
 (fn [_]
   (worker/close-worker!)
   (destroy-current)))

(defn ^:export init []
  (re-frame/dispatch-sync [::initialize-db])
  (re-frame/clear-subscription-cache!)
  (.addEventListener js/window "beforeunload"
                     (fn [event]
                       (let [show-warning @(re-frame/subscribe [:redlorry.views.settings.core/get-settings-for-key :unbooked-entries-warning])
                             unbooked-count (count @(re-frame/subscribe [:redlorry.views.timetracking.tabs.booking/new-entries]))]
                         (if (and show-warning
                                  (> unbooked-count 0))
                           (do (re-frame/dispatch [:redlorry.components.confirm-dialog/show-dialog
                                                   true
                                                   [::final-close]
                                                   "Wirklich beenden?"
                                                   "Du hast noch ungebuchte Einträge. Möchtest du RedLorry wirklich beenden?"])
                               (aset event "returnValue" false))
                           (worker/close-worker!)))))

  (u-date/set-locale moment "de")
  (reagent/render
   [main-view/redlorry]
   (.getElementById js/document "redlorry"))
  (shortcuts/register-all)
  (send-ipc "get-system-conditions"))

;;; Render redlorry as node application
(defn -main []
  (init))

(set! *main-cli-fn* -main)
