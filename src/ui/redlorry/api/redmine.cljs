(ns redlorry.api.redmine
  (:require [re-frame.core :as re-frame]
            [ajax.core :as ajax]
            [day8.re-frame.http-fx]
            [taoensso.timbre :as timbre
             :refer-macros [log  trace  debug  info  warn  error  fatal  report]]
            [redlorry.paths.redmine :as redmine-paths]
            [redlorry.utils.date :as u-date]
            [redlorry.util :as util]
            [redlorry.worker :as worker]))
;easy Debug without ui:
;set api and url with the functions and as db re-frame.db/app-db

(defn default-setup []
  (re-frame/dispatch [::url "http://localhost:3000"])
  (re-frame/dispatch [::api-key "6a4ff3011f0fbea322b78229316140edd50dd85b"]))

(defn stop-ping-func []
  (re-frame/dispatch [::stop-interval]))

(defn calc-offsets [limit total-count]
  (let [needed-steps (.ceil js/Math (/ (- total-count limit)
                                       limit))]
    (mapv #(+ % limit)
          (range 1 (+ needed-steps 1)))))

(re-frame/reg-event-db
 ::failed-request
 (fn [db [_ url resp]]
   (error "Failed request with url" url ", resp was" resp)
   db))

(re-frame/reg-event-fx
 ::start-interval
 (fn [{db :db} [_ interval]]
   (let [interval-id (.setInterval js/window
                                   (fn []
                                     (re-frame/dispatch [::ping-redmine]))
                                   interval)
         interval-id-db (get-in db redmine-paths/interval)]
     (when interval-id-db
       (.clearInterval js/window interval-id-db))
     {:db (assoc-in db
                    redmine-paths/interval
                    interval-id)
      :dispatch [::ping-redmine]})))

(re-frame/reg-event-db
 ::stop-interval
 (fn [db _]
   (let [interval-id (get-in db redmine-paths/interval)]
     (.clearInterval js/window interval-id)
     (-> db
         (util/dissoc-in redmine-paths/interval)
         (util/dissoc-in redmine-paths/user-infos)))))

(re-frame/reg-event-fx
 ::ping-success
 (fn [{db :db} [_]]
   {:db (assoc-in db
                  redmine-paths/connection-status
                  {:status true
                   :info "Verbunden"})
    :dispatch [::current-user-info]}))

(re-frame/reg-event-db
 ::ping-failed
 (fn [db [_ resp]]
   (error "Failed to ping redmine, resp:" resp)
   (assoc-in db
             redmine-paths/connection-status
             {:status false
              :info (if (= (:status resp)
                           401)
                      "Nicht autorisiert."
                      (:last-error resp))})))

(re-frame/reg-event-fx
 ::ping-redmine
 (fn [{db :db} _]
   (let [url (get-in db redmine-paths/url)]
     (if (seq url)
       {:http-xhrio {:method :get
                     :uri (str url "/users/current.json")
                     :headers {:X-Redmine-API-Key (get-in db redmine-paths/api-key)}
                     :response-format (ajax/raw-response-format)
                     :on-success [::ping-success]
                     :on-failure [::ping-failed]
                     :on-error [::ping-failed]}}
       {:db (assoc-in db
                      redmine-paths/connection-status
                      {:status :false
                       :info "No URL set."})}))))

(re-frame/reg-sub
 ::ping-status
 (fn [db _]
   (get-in db redmine-paths/connection-status)))

(re-frame/reg-sub
 ::api-key
 (fn [{db :db} _]
   (get-in db
           redmine-paths/api-key
           "")))

(re-frame/reg-event-fx
 ::set-api-key
 (fn [{db :db} [_ new-key]]
   (let [start-interval? (and (seq new-key)
                              (seq (get-in db
                                           redmine-paths/url)))]
     {:db (assoc-in db redmine-paths/api-key new-key)
      :dispatch-n [(when start-interval?
                     [::start-interval 60000])]})))

(re-frame/reg-sub
 ::url
 (fn [db]
   (get-in db
           redmine-paths/url
           "")))

(re-frame/reg-event-fx
 ::set-url
 (fn [{db :db} [_ new-url]]
   (let [start-interval? (and (seq new-url)
                              (seq (get-in db
                                           redmine-paths/api-key)))]
     {:db (assoc-in db
                    redmine-paths/url
                    new-url)
      :dispatch-n [(when start-interval?
                     [::start-interval 60000])]})))

(defn project-name [db project-id]
  (some (fn [{:keys [id name]}]
          (when (= id project-id)
            name))
        (get-in db redmine-paths/projects)))

(defn query-name [db query-id]
  (some (fn [{:keys [id name]}]
          (when (= id query-id)
            name))
        (get-in db redmine-paths/queries)))

(defn activity-name [db activity-id]
  (some (fn [{:keys [id name]}]
          (when (= id activity-id)
            name))
        (get-in db redmine-paths/time-entry-activities)))

(defn issue-name [db issue-id]
  (some (fn [{:keys [id subject]}]
          (when (= id issue-id)
            subject))
        (first (vals (get-in db redmine-paths/issues-root)))))

(re-frame/reg-sub
 ::projects
 (fn [db _]
   (get-in db redmine-paths/projects)))

(re-frame/reg-event-db
 ::success-list-projects
 (fn [db [_ resp]]
   (let [project-infos (select-keys resp [:total_count :offset :limit])]
     ;(info "Other project infos:" project-infos)
     (assoc-in db
               redmine-paths/projects
               (:projects resp)))))

(re-frame/reg-event-fx
 ::list-projects
 (fn [{db :db} _]
   (let [url (get-in db redmine-paths/url)]
     {:http-xhrio {:method :get
                   :uri (str url "/projects.json")
                   :headers {:X-Redmine-API-Key (get-in db redmine-paths/api-key)}
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success [::success-list-projects]
                   :on-failure [::failed-request (str url "/projects.json")]}})))

(re-frame/reg-event-db
 ::success-user-info
 (fn [db [_ resp]]
   (let [user-info (-> (get resp :user)
                       (select-keys [:lastname :mail :firstname :status :id]))]
     (assoc-in db
               redmine-paths/user-infos
               user-info))))

(re-frame/reg-event-fx
 ::current-user-info
 (fn [{db :db} _]
   (let [url (get-in db redmine-paths/url)]
     {:http-xhrio {:method :get
                   :uri (str url "/users/current.json")
                   :headers {:X-Redmine-API-Key (get-in db
                                                        redmine-paths/api-key
                                                        db)}
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success [::success-user-info]
                   :on-failure [::failed-request (str url "/users/current.json")]}})))

(re-frame/reg-event-db
 ::success-time-entry-activities
 (fn [db [_ resp]]
   (assoc-in db
             redmine-paths/time-entry-activities
             (get resp :time_entry_activities))))

(re-frame/reg-event-fx
 ::list-time-entry-activities
 (fn [{db :db} _]
   (let [url (str (get-in db redmine-paths/url)
                  "/enumerations/time_entry_activities.json")]
     {:http-xhrio {:method :get
                   :uri url
                   :headers {:X-Redmine-API-Key (get-in db
                                                        redmine-paths/api-key)}
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success [::success-time-entry-activities]
                   :on-failure [::failed-request url]}})))

(re-frame/reg-sub
 ::time-entry-activities
 (fn [db _]
   (get-in db
           redmine-paths/time-entry-activities)))

(re-frame/reg-sub
 ::time-entries
 (fn [db [_ period]]
   (get-in db
           (redmine-paths/time-entries period))))

(re-frame/reg-event-fx
 ::list-all-time-entries
 (fn [{db :db} [_ assoc-id]]
   (let [url (str (get-in db redmine-paths/url) "/time_entries.json")
         api-key (get-in db redmine-paths/api-key)
         user-id (get-in db (conj redmine-paths/user-infos :id))
         worker-params {:api-key api-key
                        :user-id user-id
                        :assoc-id assoc-id
                        :url url
                        :offset 0
                        :limit 100}]
     (worker/message! "load-time-entries" (clj->js worker-params))
     {})))

(re-frame/reg-event-fx
 ::list-perdiod-time-entries
 (fn [{db :db} [_ period custom-value assoc-id]]
   (let [url (str (get-in db redmine-paths/url) "/time_entries.json")
         spent-on (case period
                    :today (u-date/d-format (u-date/today))
                    :current-week (u-date/filter-string (u-date/current-week))
                    :current-month (u-date/filter-string (u-date/current-month))
                    :current-year (u-date/filter-string (u-date/current-year))
                    :custom-period (u-date/filter-string custom-value)
                    :custom-day custom-value)
         api-key (get-in db redmine-paths/api-key)
         user-id (get-in db (conj redmine-paths/user-infos :id))
         worker-params {:api-key api-key
                        :user-id user-id
                        :assoc-id assoc-id
                        :url url
                        :offset 0
                        :limit 100
                        :period period
                        :spent-on-filter spent-on}]
     (worker/message! "load-time-entries" (clj->js worker-params))
     {})))

(re-frame/reg-event-db
 ::set-time-entries
 (fn [db [_ period time-entries]]
   (assoc-in db
             (redmine-paths/time-entries period)
             time-entries)))

(defn time-entries-loaded [event result]
  (let [{:keys [time-entries assoc-id]} (js->clj result :keywordize-keys true)]
    ;short timeout for non flicking loading screen on small entries size
    (js/setTimeout #(re-frame/dispatch [::set-time-entries assoc-id time-entries])
                   500)))


(util/on-ipc "time-entries-loaded" time-entries-loaded)

;time-entries must be a vector of time-entry
;one time-entry is a map example:

(re-frame/reg-event-fx
 ::success-book-entry
 (fn [_ [_ eid]]
   {:dispatch [:redlorry.views.timetracking.tabs.booking/entry-booked eid true]}))

(re-frame/reg-event-fx
 ::failed-book-entry
 (fn [_ [_ eid entry resp]]
   (info "booking failed  :/ ")
   (info entry)
   (info "FAILED" resp)
   {:dispatch [:redlorry.views.timetracking.tabs.booking/entry-booked eid false]}))


#_{:issue_id 0
   :spent_on "DD-MM-YYYY"
   :hours "0.5"
   :activity_id 0
   :comments ""}
(re-frame/reg-event-fx
 ::book-time-entries
 (fn [{db :db} [_ time-entries]]
   (let [url (str (get-in db redmine-paths/url) "/time_entries.json")
         api-key (get-in db redmine-paths/api-key)]
     {:http-xhrio (mapv (fn [[eid entry]]
                          (let [time-entry {:time_entry (-> entry
                                                            (assoc :issue_id (js/parseInt (get entry :issue_id)))
                                                            (assoc :activity_id (js/parseInt (get entry :activity_id))))}]
                            {:method :post
                             :uri url
                             :headers {:X-Redmine-API-Key api-key}
                             :response-format (ajax/json-response-format {:keywords? true})
                             :format (ajax/json-request-format)
                             :params time-entry
                             :on-success [::success-book-entry eid]
                             :on-failure [::failed-book-entry eid time-entry]}))
                        time-entries)})))

(re-frame/reg-event-db
 ::success-list-queries
 (fn [db [_ resp]]
  ; (let [queries-infos (select-keys resp [:total_count :offset :limit])]
     ;(info "Other queries infos:" queries-infos)
   (assoc-in db
             redmine-paths/queries
             (get resp :queries))))

(re-frame/reg-event-fx
 ::list-queries
 (fn [{db :db} _ project-id]
   (let [url (str (get-in db redmine-paths/url) "/queries.json")
         params-map (if project-id
                      {:project-id project-id}
                      {})]
     {:http-xhrio {:method :get
                   :uri url
                   :params params-map
                   :headers {:X-Redmine-API-Key (get-in db redmine-paths/api-key)}
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success [::success-list-queries]
                   :on-failure [::failed-request url]}})))

(re-frame/reg-sub
 ::queries-for-project
 (fn [db [_ project]] ;the complete project-map
   (if project
     (let [project-id (get project :id)]
       (filterv (fn [q]
                  (or (nil? (get q :project_id))
                      (= (get q :project_id)
                         project-id)))
                (get-in db redmine-paths/queries)))
     (get-in db redmine-paths/queries))))

;If only project-id is set, then all issues from the project gets requested.
;When the query-id is set then all issues with the query = true gets requested.
;Is none set all issues getting requested (can take a while).
;Combing project-id and query-id is possible but only the query-id gets used to save the issues in the app-state.
(re-frame/reg-event-fx
 ::list-issues
 (fn [{db :db} [_ args-map]]
   (let [url (str (get-in db redmine-paths/url) "/issues.json")
         worker-params {:api-key (get-in db redmine-paths/api-key)
                        :url url
                        :offset 0
                        :limit 100
                        :args-map args-map}]
     (info "Async-loading issues")
     (worker/message! "load-issues" (clj->js worker-params))
     {})))

(re-frame/reg-event-db
 ::set-issues
 (fn [db [_ issues-qid issues]]
   (assoc-in db
             (redmine-paths/issues issues-qid)
             issues)))

(defn issues-loaded [event result]
  (let [conv-res (js->clj result :keywordize-keys true)
        [iss-id issues] (first conv-res)]
    (re-frame/dispatch [::set-issues (name iss-id) issues])))

(util/on-ipc "issues-loaded" issues-loaded)

(re-frame/reg-sub
 ::issues
 (fn [db [_ {:keys [project-id query-id]}]]
   (cond
     (and query-id project-id) (get-in db
                                       (redmine-paths/issues (str project-id "-" query-id)))
     query-id (get-in db
                      (redmine-paths/issues query-id))
     project-id (get-in db
                        (redmine-paths/issues project-id))
     :else (mapcat (fn [[_ issues]]
                     issues)
                   (get-in db
                           redmine-paths/issues-root)))))
