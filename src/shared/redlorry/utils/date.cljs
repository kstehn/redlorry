(ns redlorry.utils.date
  (:require [moment :as moment]
            [moment.locale.de]
            [clojure.string :as clj-str]))

(def months (atom []))
(def week-days (atom []))
(def week-days-short (atom []))
(def week-numbers (atom []))

(defn set-locale [mom loc]
  (.locale mom loc))

(defn get-months []
  (if (empty? @months)
    (reset! months (js->clj (moment.months)))
    @months))

(defn days-of-month [month year]
  (let [month-idx (if (string? month)
                    (.indexOf (get-months)
                              month)
                    month)
        mom-obj (-> (moment)
                    (.date "1")
                    (.month month-idx)
                    (.year year))
        month-start (.startOf (.clone mom-obj)
                              "month")
        month-end (.endOf (.clone mom-obj)
                          "month")]
    (vec (range (.date month-start)
                (+ 1 (.date month-end))))))

(defn today []
  (moment.))

(defn d-format [d]
  (.format d "YYYY-MM-DD"))

(defn day [day month year]
  (let [day-str (-> (moment)
                    (.year year)
                    (.month month)
                    (.date day)
                    d-format)]
    day-str))

(defn current-week []
  (let [start-week (-> (today)
                       (.clone)
                       (.startOf "week")
                       d-format)
        end-week (-> (today)
                     (.clone)
                     (.endOf "week")
                     d-format)]
    [start-week end-week]))

(defn week-range [week month year]
  (let [w-start-day (+ 1 (* 7 (- week 1)))
        mom-obj (-> (moment)
                    (.year year)
                    (.month month)
                    (.date w-start-day))
        start-week (-> mom-obj
                       (.clone)
                       (.startOf "week"))
        start-week (if (= month (.month start-week))
                     (d-format start-week)
                     (d-format (-> mom-obj
                                   (.clone)
                                   (.startOf "month"))))
        end-week (-> mom-obj
                     (.clone)
                     (.endOf "week"))
        end-week (if (= month (.month end-week))
                   (d-format end-week)
                   (d-format (-> mom-obj
                                 (.clone)
                                 (.endOf "month"))))]
    [start-week end-week]))

(defn current-month []
  (let [start-month (-> (today)
                        (.clone)
                        (.startOf "month")
                        d-format)
        end-month (-> (today)
                      (.clone)
                      (.endOf "month")
                      d-format)]
    [start-month end-month]))

(defn month-range [month year]
  (let [mom-obj (-> (moment)
                    (.year year)
                    (.month month))
        start-month (-> mom-obj
                        (.clone)
                        (.startOf "month")
                        d-format)
        end-month (-> mom-obj
                      (.clone)
                      (.endOf "month")
                      d-format)]
    [start-month end-month]))

(defn current-year []
  (let [start-year (-> (today)
                       (.clone)
                       (.startOf "year")
                       d-format)
        end-year (-> (today)
                     (.clone)
                     (.endOf "year")
                     d-format)]
    [start-year end-year]))

(defn year-range [year]
  (let [mom-obj (-> (moment)
                    (.year year))
        start-year (-> mom-obj
                       (.clone)
                       (.startOf "year")
                       d-format)
        end-year (-> mom-obj
                     (.clone)
                     (.endOf "year")
                     d-format)]
    [start-year end-year]))


(defn filter-string [[start-date end-date]]
  (str "><" start-date "|" end-date))

(defn get-week-days
  ([]
   (get-week-days false))
  ([ger-format?]
   (if ger-format?
     (conj (vec (rest (js->clj (moment.weekdays))))
           (first (js->clj (moment.weekdays))))
     (vec (js->clj (moment.weekdays))))))

(defn get-week-days-short
  ([]
   (get-week-days-short false))
  ([ger-format?]
   (if ger-format?
     (conj (vec (rest (js->clj (moment.weekdaysMin))))
           (first (js->clj (moment.weekdaysMin))))
     (vec (js->clj (moment.weekdaysMin))))))

(defn get-week-dates
  ([d]
   (let [start-mom (moment d)
         start-mom (-> start-mom
                       (.startOf "week"))
         start-mom (.subtract start-mom
                              1 "d")]
     (mapv (fn [ind]
             (.format (.add start-mom 1 "d")
                      "YYYY-MM-DD"))
           (range 0 7))))
  ([]
   (let [start-of-week (.subtract (.startOf  (moment.) "isoWeek")
                                  1 "d")]
     (mapv (fn [ind]
             (.format (.add start-of-week 1 "d"
                            "YYYY-MM-DD")))
           (range 0 7)))))

(defn get-month-weeks []
  (if (empty? @week-numbers)
    (let [month-start (.startOf (moment.)
                                "month")
          month-end (.endOf (moment.)
                            "month")
          week-count (+ 1 (.weeks (moment. (- month-end month-start))))]

      (reset! week-numbers (mapv (fn [num]
                                   num)
                                 (range 1 week-count))))
    @week-numbers))

(defn month-weeks [month-str year-str]
  (let [month-idx (.indexOf (get-months)
                            month-str)
        mom-obj (-> (moment)
                    (.date "1")
                    (.month month-idx)
                    (.year year-str))
        month-start (.startOf (.clone mom-obj)
                              "month")
        month-end (.endOf (.clone mom-obj)
                          "month")
        week-count (+ 1 (.weeks (moment. (- month-end month-start))))]
    (vec (range 1 week-count))))

(defn get-month-name [date-str]
  (.format (moment. date-str
                    "YYYY-MM-DD")
           "MMMM"))

(defn get-month-week-for-date [date-str]
  (let [mom-obj (moment. date-str
                         "YYYY-MM-DD")]
    (- (+ 1 (.week mom-obj))
       (.week (.startOf mom-obj
                        "month")))))

(defn translate-to-new-date-str [date-str old-format new-format]
  (.format (moment. date-str old-format)
           new-format))

;Format timestr = hh:mm, result x.y
(defn time-str->number [time-str]
  (let [split (clj-str/split time-str #":")
        hours (js/parseFloat (first split))
        minutes (js/parseFloat (second split))]
    (+ hours
       (js/parseFloat (.toFixed (/ minutes 60)
                                2)))))

(defn number->time-str [time-number]
  (let [hours (js/Math.trunc time-number)
        minutes (* (- time-number hours)
                   60)
        hours-str (if (> 10 hours)
                    (str "0" hours)
                    hours)
        minutes-str (if (> 10 minutes)
                      (str "0" minutes)
                      minutes)]
    (str hours-str ":" minutes-str)))

(defn get-time
  ([time-str round-min add-hours time-str?]
   (let [[hours minutes] (clj-str/split time-str #":")
         mom-obj (-> (moment)
                     (.hours hours)
                     (.minutes minutes))]
     (get-time mom-obj round-min add-hours)))
  ([mom-obj round-min add-hours]
   (let [mod-min (when (> round-min 0)
                   (mod (.minutes mom-obj)
                        round-min))
         remainder (if (or
                        (nil? mod-min)
                        (= mod-min 0)
                        (= round-min 0))
                     0
                     (- round-min mod-min))
         datetime (-> mom-obj
                      (.clone)
                      (.add remainder "minutes")
                      (.add add-hours "hours"))]
     (.format datetime "HH:mm"))))

(defn get-curr-time
  ([]
   (get-curr-time 0))
  ([round-min]
   (get-curr-time round-min 0))
  ([round-min add-hours]
   (let [curr-time (moment.)]
     (get-time curr-time round-min add-hours))))

(defn to-month [date-str]
  (-> (moment date-str)
      (.month)))

(defn is-between? [check-date start-date end-date]
  (.isBetween (moment check-date)
              start-date
              end-date
              nil
              "[]")) ;inclusion