(ns redlorry.data
  (:require [fs :refer [writeFileSync readFileSync readFile]]
            [redlorry.util :refer [get-path get-app]]))

(defn db-path
  "In Dev and Win: %APPDATA%/Electron
   In Prod and Win: %APPDATE%/redlorry"
  []
  (get-path "userData" "redlorry.json"))

(defn json-parse
  [string]
  (.parse js/JSON string))

(defn json-stringify
  [data]
  (.stringify js/JSON data))

(defn read
  ([path]
   (try
     (-> path
         readFileSync
         json-parse
         (js->clj :keywordize-keys true))
     (catch js/Object e
       {})))
  ([]
   (read (db-path))))

(defn read-async [callback]
  (-> (db-path)
      (readFile (fn [err data]
                  (if err
                    (println err)
                    (callback (-> data
                                  json-parse
                                  (js->clj :keywordize-keys true))))))))

(defn write-file-sync
  [data path]
  (writeFileSync path data))

(defn write
  [data]
  (-> (read)
      (merge data)
      clj->js
      json-stringify
      (write-file-sync (db-path))))

(defn write-path
  [data path]
  (-> data
      clj->js
      json-stringify
      (write-file-sync path)))
