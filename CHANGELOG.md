## **WIP: 2.1.2**
###### *TODO Release date*

* TODO

## 2.1.1
###### *20.11.2019*

* Build 32 Bit -> 64 Bit
* Timetracking: Möglichkeit zum zurücksetzen auf das aktuelle Datum
* Bug Fix Auto Updater:
  * Handle Exception when fetching new Release and show Message if installation failed
  * Check if there are Update Infos in UI before showing Update UI
* Bug Fix Timetracking:
  * Datepicker: Bei Monaten mit 5 Wochen, konnten die letzten Tage nicht richtig selektiert werden
* Bug Fix Shortcuts:
  * Strg + Shift + S: Eintrag speichern und weiteren erstellen hat die Defaults nicht neuberechnet
  * Strg + A: Verhinderte Default-Verhalten in Kommentarfeld und Settings Textfeldern
* Dependencies hochgezogen:
  * org.clojure/clojurescript 1.10.520 -> 1.10.597
  * org.clojure/core.async 0.4.500 -> 0.5.527
  * electron 6.0.5 -> 7.1.2
  * ws 7.1.2 -> 7.2.0
  * re-frame 0.10.6-> 0.11.0-rc2
  * cljsjs/react + react-dom 16.8.6-0 -> 16.11.0-0
  * cljsjs/material-ui 4.4.0-0 -> 4.5.1-0
  * cljsjs/material-ui-icons 4.2.1 -> 4.4.1-0
  * cljsjs/chartjs 2.8.0-0 -> 2.9.2-0
  * binaryage/devtools 0.9.10 -> 0.9.11

## 2.1.0
###### *10.09.2019*

* Allgemein:
  * Design von Scrollbalken verändert
  * Anzeige für den Redmine Verbinungsstatus: Bei Klick auf das Icon wird die Redmine-Seite geöffnet
  * Bug Fix: Darstellung von Navigation Tooltips 
  * Bug Fix: RedLorry wirft keine Exception mehr, wenn keine Internetverbindung vorhanden ist
  * Status Anzeige verbessert
  * Warnung beim Schließen von RedLorry, wenn noch ungebuchte Einträge vorhanden sind: Per Setting deaktivierbar
* Timetracking:
  * Mehr Queries zur Auswahl möglich bei Tag, Woche, Monat, Jahr
  * Bessere Anfragestrategie - Einträge werden nur neu geladen, wenn nötig
  * Summen-Zeile leicht überarbeitet (Icons, Schrift, Positionierung)
  * Stunden pro Monat hinzugefügt in der Jahr-Ansicht
  * Anzeige von der Anzahl ungebuchten Einträgen in der App Bar
  * Eintrag Dialog:
    * Default Von-Zeit im Eintrags-Dialog nun konfigurierbar (Default 09:30)
    * Bis-Zeit ist nun die aktuelle Zeit
  * Shortcuts hinzugefügt:
    * `strg+n`: Neuer Eintrag Dialog (Bedingung: Timetracking Ansicht)
    * `strg+a`: Alle ungebuchten Einträge selektieren/deselektieren (Bedingung: Booking Übersicht))
    * `strg+b`: Alle selektierten Einträge buchen (Bedingung: Booking Übersicht & mind. 1 Eintrag selektiert)
    * `strg+d`: Alle selektierten Einträge löschen (Bedingung: Booking Übersicht & mind. 1 Eintrag selektiert)
    * `strg+s`: Speichert neuen Eintrag (Bedingung: Neuer Eintrag Dialog)
    * `strg+shift+s`: Speichert neuen Eintrag und öffnet weiteren Dialog (Bedingung: Neuer Eintrag Dialog)
    * `strg+q`: Schließt neuer Eintrag Dialog (Bedingung: Neuer Eintrag Dialog)
    * `strg+1`: Wechsel zu Zeiterfassung
    * `strg+2`: Wechsel zu Einstellungen
    * `strg+3`: Shortcuts Übersicht öffnen
  * Export von Einträgen hinzugefügt:
    * Excel - shortcut: `ctrl+e e`
    * Csv - shortcut: `ctrl+e c`
  * Bug Fix: Statusmeldungen erscheinen nun wieder nachdem gebucht wurde
  * Bug Fix: Ein Eintrag wurde zu wenig angefragt, wenn die Gesamtzahl über 100 lag
  * Bug Fix: Kurzes flackern des Loading Screens bei wenig Daten
* Shortcuts Ansicht hinzugefügt:
  * Anzeige aller Shortcuts
  * Änderung aller Shortcuts
  * Zurücksetzen auf Default
* Settings:
  * Settings Design leicht überarbeitet
  * Es wird nur gespeichert und angefragt, wenn die Werte auch verändert wurden sind
  * Alle Bereiche bleiben sichtbar, auch wenn keine Redmine Authentifizierung vorhanden ist. In diesem Fall werden in timetracking alle Tabs deaktiviert
  * Bug Fix: Korrektes setzen des API-Keys nach einer Änderung
* Dependencies hochgezogen:
  * electron 5.0.5 -> 6.0.5
  * ws 7.0.1 -> 7.1.2
  * cljsjs/material-ui 4.1.0 -> 4.4.0-0
  * cljsjs/material-ui-icons 3.0.1 -> 4.2.1

## 2.0.25
###### *24.06.2019*

* Schnellere initiale Startzeit durch deutlich verringerte Bildergrößen
* Fix Notifications: Flackern beim Speichern von Settings
* Fix Selects: Darstellung von geöffneten Selects ohne Options
* Dependencies hochgezogen:
  * cljsjs/moment-range 4.0.1-0 -> 4.0.2-0
  * cljsj/material-ui 3.9.3 -> 4.1.0
  * cljsjs/react 16.8.3-0 -> 16.8.6-0
  * cljsjs/react-dom 16.8.3 -> 16.8.6-0
  * org.clojure/core.async 0.4.490 -> 0.4.500
  * org.clojure/clojure 1.10.0 -> 1.10.1
  * electron 5.0.1 -> 5.0.5
  * ws 6.2.0 -> 7.0.1

## 2.0.24
###### *05.05.2019*

* Timetracking:
  * Fix: Charts wurden nicht aktualisiert, wenn ein Chart zuvor aufgerufen wurde, danach gebucht wurde und anschließend das gleiche Chart aufgerufen wurde (did-mount Problematik, da die Entries erst später ankommen, als die UI gerendert wird)
* Memory Leak: Abräumen von Chart Objekten
* Mac Support hinzugefügt
* Updater: Check Mac Condition (Updater nicht für Mac verfügbar)
* Settings -> General Auto Updater + Update-Check: Mac conditions hinzugefügt + Meldung
* Fix: OS Erkennung
* Dependencies hochgezogen:
  * electron 4.1.1 -> 5.0.1 (NodeIntegration default false/vorher true)
  * cljsjs/chartjs 2.7.4 -> 2.8.0
  * cljsjs/react-draggable 3.2.1-0 -> 3.3.0-0
  * cljsjs/material-ui 3.9.1-0 -> 3.9.3-0

## 2.0.23
###### *01.04.2019*

*  Fix: Buchen - TimeEntries wurden nicht angezeigt
*  Fix: Buchen - TimeEntries wurden nicht gespeichert
*  Fix: Buchen - TimeEntries wurden nicht gelöscht
*  Fix: Buchen - Es konnte nicht gebucht werden
*  Fix: Buchen - Invalid Date (from wurde als Date gespeichert)
*  Fix: Buchungsdialog - From und To waren falsch beim Update
*  Fix: Buchungsdialog - Speichern und weiteren Eintrag anlegen
*  Fix: Buchungsdialog - Felder übernehmen (keep-dialog-values)
*  Update-Helper: Unterschiedliche Styles 4k/kleiner als 4k

## 2.0.22
###### *31.03.2019*

* Timetracking:
  *  Fix: Buchungs-Dialog - Wochentage/Datum um einen Tag verschoben
  *  Jahreswoche wird mit angezeigt
  *  Buchungs-Dialog Draggable
* Updater:
  *  Update-Helper UI Design verbessert
  *   Wenn kein Java vorhanden ist, werden Updates deaktiviert
* Refactoring
* Dependencies hochgezogen:
  * React-Day-Picker 5.5.3-1 -> 7.3.0-1
  * react 16.8.1-0 -> 16.8.3-0
  * react-dom 16.8.1-0 -> 16.8.3-0
  * react-draggable 3.0.3-0 -> 3.2.1-0
  * core.async 0.4.474 -> 0.4.490
  * electron 4.0.6 -> 4.1.1
  * electron-builder 20.38.5 -> 20.39.0
  * ws 3.3.1 -> 6.2.0
  * re-frame-10x 0.3.6 -> 0.3.7

## 2.0.21
###### *01.03.2019*

*  UI: Settings
   *  Keine Speicher-Buttons mehr
   *  Keine native Selects mehr
   *  Felder übernehmen: Andere Darstellung
   *  Refactoring Settings:
      *  Auslagerung Options (+ Zusammengelegt mit entry dialog options)
      *  Selects verallgemeinert
      *  Update und Save von Settings verallgemeinert
*  UI: Statusanzeige verschoben
*  Fix: Manchmal keine Issues - Sollte jetzt gehen
*  Timetracking:
   *  Es werden nur noch die Timeentries des Users abgefragt (nicht mehr alle, auf die er Zugriff hat)
   *  Timeentries werden nicht mehr vermischt, wenn man schnell durch die Tabs wechselt
*  Menüleiste automatisch in Dev sichtbar und in prod unsichtbar
*  Dependencies Update:
   *  Electron: 4.0.4 -> 4.0.6
   *  Clojurescript 1.10.516 -> 1.10.520
   *  moment-range 3.0.3-0 -> 4.0.1-0

## 2.0.20
###### *22.02.2019*

*  Fix: Settings Default Query Options - Projekt spezifische Queries wurden nciht angezeigt (public schon)
*  UI: Kein App-Menü mehr
*  UI: Wechsel zwischen den Bereichen nun dauerhaft sichtbar

## 2.0.19
###### *20.02.2019*

*  Automatischer Updatecheck beim Start von RedLorry deaktvierbar (Einstellungen -> Allgemein)
*  Fix: Dialog - Sprung des Inhalts kurz vorm Schließen
*  Fix: Redmine Settings Speichern (Auch keine Null Werte mehr)
*  Fix: Timetracking Settings - Anzeige von Projekten & co nach eintragen der Redmine Daten
*  Fix: Timetracking Settings - Query war immer disabled, wenn es einmal disabled war
*  Fix: Settings - Save Buttons nun keine icon-buttons mehr
*  Fix: Timetracking - Summe Stunden  nun auf 2 Nachkommastellen gerundet
*  Fix: RedLorry hatte einen unendlichen Ladescreen, wenn nur die Redmine Daten eingetragen waren, aber kein Default-Projekt.

## 2.0.18
###### *13.02.2019*

*  RedLorry (Auto-) Updater
   * Release Notes werden angezeigt
   * Automatisch beim Start
   * Manuell unter Settings -> General
   * Ablehnen des Updates möglich
   * Update Progress UI inkl. Neustarten von RedLorry
*  Fix Dialog Bug: Dadurch konnte RedLorry abstürzen unter bestimmen Umständen
*  Ab hier keine alpha/beta Unterscheidung mehr

## alpha-2.0.14
###### *05.02.2019*

*  Fix: Today-Datum nun auch noch am nächsten Tag korrekt
*  Version nun im Menü und nicht mehr im Header

## alpha-2.0.13
###### *02.02.2019*

*  Fix Absturz bei Fokusverlust #42
*  Fix zu viele Issues Problem: Es werden nur noch die vom Default-Projekt abgefragt
*  Timetracking basierend auf Default-Projekt
*  Warnung, wenn Default-Projekt nicht ausgewählt
*  Settings: Timetracking deaktiviert, wenn keine Redmine Daten eingetragen
*  Settings: Allgemein deaktivert, wenn kein Default-Projekt eingetragen ist
*  Fix Material UI: variant subheading deprecated
*  Kein Aufruf mehr von load-init bei Tabwechsel

## alpha-2.0.10
###### *29.01.2019*

*  Fix: Warning Table (deprecated :numeric...)
*  Fix: Bei edit wird kein neuer Eintrag mehr erstellt, sondern der alte verändert
*  Fix: Bei edit und vorher selektiert, bleibt die selektierung auf dem richtigen Eintrag vorhanden
*  Fix: (experimentell) Beim Starten wird auf Queries und Issues gewartet, bevor Redlorry benutzbar ist (Nach 5 versuchen wird die UI freigegeben, selbst wenn sie nicht geladen wurden)
*  Fix: Es ist nur noch die Checkbox anklickbar, sodass man auf einer Zeile des Buchungseintrags klicken kann ohne zu selektieren
*  Feature: Bei einem weiteren Buchungseintrag wird nun die to-Zeit zur neuen from-zeit, wenn zuvor Uhrzeiten eingetragen wurden sind

## alpha-2.0.7
###### *26.01.2019*

*  Material-UI 3.9.1

## alpha-2.0.6
###### *26.01.2019*

*  Stundenauswahl möglich
*  Pause eintragen
*  Pause und Stunde wird in Booking Table angezeigt
*  Config für Stundenauswahl Switch
*  Date Switch anordnung geändert
*  Material-ui: 3.9.0
*  Durchschnitt Stunden anzeigen
