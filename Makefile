.DEFAULT_GOAL := build

first:
	npm install -g electron-packager

install:
	npm install

compile:
	lein build

clean:
	rm -rf out
	rm -rf prepared
	rm -rf resources/app
	rm -rf resources/main.js
	lein clean

prepare:
	lein prepare-release
	mkdir prepared/node_modules
	cp -R node_modules/react prepared/node_modules/react/
	cp -R node_modules/react-dom prepared/node_modules/
	cp -R node_modules/create-react-class prepared/node_modules/
	cp -R node_modules/fbjs prepared/node_modules/
	cp -R node_modules/loose-envify prepared/node_modules/
	cp -R node_modules/object-assign prepared/node_modules/
	cp -R node_modules/electron-default-menu prepared/node_modules/
	cp -R node_modules/electron prepared/node_modules/
	cp -R node_modules/xmlhttprequest prepared/node_modules/

build: clean install compile prepare
	cd prepared/node_modules/react/ && rm -rf dist
	cd prepared/ && electron-packager . RedLorry --out=../out --ignore='\b(target|out|dev|Procfile~?|update-helper|node_modules\/electron\/dist)\b' --ignore='[.](clj|log)' --ignore='~$$' --ignore='\b(nod e_modules\/(?!react|react-dom|create-react-class|fbjs|loose-envify|object-assign|electron-default-menu|electron|prop-types|xmlhttprequest))\b' --prune=false --overwrite --arch=x64
	for dest in out/* ; do cp update-helper.jar $$dest/new-update-helper.jar ; done  

build-all: clean install compile prepare
	cd prepared/node_modules/react/ && rm -rf dist
	cd prepared/ && electron-packager . RedLorry --out=../out --ignore='\b(target|out|dev|Procfile~?|update-helper|node_modules\/electron\/dist)\b' --ignore='[.](clj|log)' --ignore='~$$' --ignore='\b(nod e_modules\/(?!react|react-dom|create-react-class|fbjs|loose-envify|object-assign|electron-default-menu|electron|prop-types|xmlhttprequest))\b' --prune=false --overwrite --platform=win32 --arch=x64
	cd prepared/ && electron-packager . RedLorry --out=../out --ignore='\b(target|out|dev|Procfile~?|update-helper|node_modules\/electron\/dist)\b' --ignore='[.](clj|log)' --ignore='~$$' --ignore='\b(nod e_modules\/(?!react|react-dom|create-react-class|fbjs|loose-envify|object-assign|electron-default-menu|electron|prop-types|xmlhttprequest))\b' --prune=false --overwrite --platform=linux --arch=x64
	cd prepared/ && electron-packager . RedLorry --out=../out --ignore='\b(target|out|dev|Procfile~?|update-helper|node_modules\/electron\/dist)\b' --ignore='[.](clj|log)' --ignore='~$$' --ignore='\b(nod e_modules\/(?!react|react-dom|create-react-class|fbjs|loose-envify|object-assign|electron-default-menu|electron|prop-types|xmlhttprequest))\b' --prune=false --overwrite --platform=darwin
	for dest in out/* ; do cp update-helper.jar $$dest/new-update-helper.jar ; done  
	cd out/RedLorry-win32-x64 && zip -r ../../RedLorry-win.zip . 
	cd out/RedLorry-linux-x64 && zip -r ../../RedLorry-linux.zip .
	cd out/RedLorry-darwin-x64 && zip -r ../../RedLorry-mac.zip .

pack-all:
	cd prepared/node_modules/react/ && rm -rf dist
	cd prepared/ && electron-packager . RedLorry --out=../out --ignore='\b(target|out|dev|Procfile~?|update-helper|node_modules\/electron\/dist)\b' --ignore='[.](clj|log)' --ignore='~$$' --ignore='\b(nod e_modules\/(?!react|react-dom|create-react-class|fbjs|loose-envify|object-assign|electron-default-menu|electron|prop-types|xmlhttprequest))\b' --prune=false --overwrite --platform=win32 --arch=x64
	cd prepared/ && electron-packager . RedLorry --out=../out --ignore='\b(target|out|dev|Procfile~?|update-helper|node_modules\/electron\/dist)\b' --ignore='[.](clj|log)' --ignore='~$$' --ignore='\b(nod e_modules\/(?!react|react-dom|create-react-class|fbjs|loose-envify|object-assign|electron-default-menu|electron|prop-types|xmlhttprequest))\b' --prune=false --overwrite --platform=linux --arch=x64
	cd prepared/ && electron-packager . RedLorry --out=../out --ignore='\b(target|out|dev|Procfile~?|update-helper|node_modules\/electron\/dist)\b' --ignore='[.](clj|log)' --ignore='~$$' --ignore='\b(nod e_modules\/(?!react|react-dom|create-react-class|fbjs|loose-envify|object-assign|electron-default-menu|electron|prop-types|xmlhttprequest))\b' --prune=false --overwrite --platform=darwin
	for dest in out/* ; do cp update-helper.jar $$dest/new-update-helper.jar ; done  
	cd out/RedLorry-win32-x64 && zip -r ../../RedLorry-win.zip . 
	cd out/RedLorry-linux-x64 && zip -r ../../RedLorry-linux.zip .
	cd out/RedLorry-darwin-x64 && zip -r ../../RedLorry-mac.zip .

.PHONY: install
